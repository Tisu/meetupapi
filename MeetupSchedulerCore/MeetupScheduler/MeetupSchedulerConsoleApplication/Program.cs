﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using MeetupApi.MeetupClient;
using MeetupSchedulerConsoleApplication.Config;
using Microsoft.Extensions.Configuration;
using RsvpScheduler;
using RsvpScheduler.Builder;

[assembly: log4net.Config.XmlConfigurator(Watch = true, ConfigFile = "log4net.config")]
namespace MeetupSchedulerConsoleApplication
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var logger = LogManager.GetLogger(typeof(Program));
            logger.Error("Program starting");

            var configuration = GetConfiguration();
            var credential = new NetworkCredential(configuration.GetRsvpSchedulerEmailUser(),
                                                   configuration.GetRsvpSchedulerEmailPassword());
            var email = new MailAddress(configuration.GetEmailAddress());
            var emailConfiguration = new EmailConfiguration(email, credential);

            try
            {
                var scheduler = await SchedulerBuilder.Create(configuration.GetMeetupKey(), AuthenticationType.ApiKey)
                .SetLogger(logger)
                .UseUserInformationFromMeetup()
                .ExcludeGroup("JS-Upskill-Wroclaw")
                .ExcludeGroup("Studenckie-Grupy-NET")
                .ExcludeGroup("SysOpsPolska")
                .SendInformationToEmail(configuration.GetEmailAddress())
                .SignForFutureRsvpInfinitely()
                .DontSignForEventsInThePast()
                .NotifyClientsWithCredentials(emailConfiguration)
                .Build("test");

                scheduler.ScheduleRsvpForNextMeeting();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            new ManualResetEvent(false).WaitOne();
            logger.Error("Program terminated");
        }

        public static Configuration GetConfiguration()
        {
            var configurationRoot = new ConfigurationBuilder()
                .AddJsonFile("config.json")
                .Build();

            var configuration = new Configuration(configurationRoot);
            return configuration;
        }
    }
}
