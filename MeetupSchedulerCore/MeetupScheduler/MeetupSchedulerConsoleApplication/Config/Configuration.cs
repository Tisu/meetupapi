﻿using Microsoft.Extensions.Configuration;

namespace MeetupSchedulerConsoleApplication.Config
{
    public class Configuration
    {
        private readonly IConfigurationRoot _configurationRoot;

        public Configuration(IConfigurationRoot configurationRoot)
        {
            _configurationRoot = configurationRoot;
        }
        public string GetEmailAddress()
        {
            return _configurationRoot["EmailToNotify"];
        }
        public string GetMeetupKey()
        {
            return _configurationRoot["MeetupKey"];
        }
        public string GetRsvpSchedulerEmail()
        {
            return _configurationRoot.GetSection("RsvpEmailNotification")["Email"];
        }
        public string GetRsvpSchedulerEmailUser()
        {
            return _configurationRoot.GetSection("RsvpEmailNotification").GetSection("Credentials")["User"];
        }
        public string GetRsvpSchedulerEmailPassword()
        {
            return _configurationRoot.GetSection("RsvpEmailNotification").GetSection("Credentials")["Password"];
        }
    }
}