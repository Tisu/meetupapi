﻿using System.Threading;
using System.Threading.Tasks;
using log4net;
using MeetupApi.MeetupClient;
using MeetupEmailSender.Messages;
using Quartz;
using RsvpScheduler.QuartzKeys;

namespace RsvpScheduler.Jobs
{
    public sealed class MeetupRsvpJob : IJob
    {
        public const int RetryRsvpOnFailure = 5;
        public Task Execute(IJobExecutionContext context)
        {
            var dataMap = context.MergedJobDataMap;

            var eventId = dataMap.GetString(MeetupJobKeys.EventId);
            var meetupClient = dataMap.Get(MeetupJobKeys.MeetupClient) as IMeetupClient;
            var retries = 0;
            var logger = dataMap.Get(MeetupJobKeys.Logger) as ILog;
            
            //todo change for Polly
            while(retries < RetryRsvpOnFailure)
            {
                var result = meetupClient?.PostRsvpAsync(eventId).Result;

                if(!result.HasValue || !result.Value)
                {
                    logger?.Error($"Rsvp post failed for: {context.JobDetail.Key} trying again ");
                    Thread.Sleep(300);
                    retries++;
                    continue;
                }

                logger?.Debug($"Rsvp post successful for: {context.JobDetail.Key}");
                var successfulMessage = dataMap.Get(MeetupJobKeys.SuccessfullMessage) as ISuccessfullRsvpMessage;
                successfulMessage?.SendSuccessfulRsvpNotification();
                break;
            }
            return Task.CompletedTask;
        }
    }
}