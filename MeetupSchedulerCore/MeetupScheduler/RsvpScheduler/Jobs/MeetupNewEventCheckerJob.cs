﻿using System.Threading.Tasks;
using log4net;
using Quartz;
using RsvpScheduler.QuartzKeys;
using RsvpScheduler.Scheduler;

namespace RsvpScheduler.Jobs
{
    public sealed class MeetupNewEventCheckerJob : IJob
    {
        public Task Execute(IJobExecutionContext context)
        {
            var logger = context.MergedJobDataMap.Get(MeetupJobKeys.Logger) as ILog;

            logger?.Debug($"Checking status for {context.JobDetail.Key}");
            var rsvpScheduler = context.MergedJobDataMap.Get(MeetupJobKeys.RsvpScheduler) as IRsvpScheduler;
            rsvpScheduler?.ScheduleRsvpForNextMeeting();

            return Task.CompletedTask;
        }
    }
}