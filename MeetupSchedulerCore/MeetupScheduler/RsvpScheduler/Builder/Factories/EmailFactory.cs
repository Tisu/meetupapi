﻿using System.Net.Mail;
using log4net;
using MeetupEmailSender.MeetupEmailSender;

namespace RsvpScheduler.Builder.Factories
{
    public sealed class EmailFactory
    {
        public static IMeetupEmailSender Create(string email, ILog logger, EmailConfiguration emailConfiguration)
        {
            return string.IsNullOrEmpty(email) ? new NullMeetupEmailSender() : CreateMeetupEmailSender(email, logger, emailConfiguration);
        }

        private static IMeetupEmailSender CreateMeetupEmailSender(string email, ILog logger, EmailConfiguration emailConfiguration)
        {
            return new MeetupEmailSender.MeetupEmailSender.MeetupEmailSender(
                new EmailSender.Sender.EmailSender(
                    new MailAddress(email), emailConfiguration.Email, emailConfiguration.Credential, logger));
        }
    }
}