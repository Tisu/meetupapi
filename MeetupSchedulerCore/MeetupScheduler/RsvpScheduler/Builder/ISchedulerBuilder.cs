using System.Collections.Generic;
using System.Threading.Tasks;
using log4net;
using Quartz;
using RsvpScheduler.Scheduler;

namespace RsvpScheduler.Builder
{
    public interface ISchedulerBuilder
    {
        ISchedulerBuilder SignOnlyInCity(string city);
        ISchedulerBuilder SendInformationToEmail(string myEmail);
        ISchedulerBuilder DontSignForEventsInThePast();
        ISchedulerBuilder AddGroupToRsvp(string groupUrl);
        ISchedulerBuilder AddGroupToRsvp(IEnumerable<string> groupsUrl);
        ISchedulerBuilder SignForFutureRsvpInfinitely();
        ISchedulerBuilder SendInformationToEmailRetrievedFromMeetup();
        ISchedulerBuilder UseUserGroupsFromMeetup();
        ISchedulerBuilder UseUserCityFromMeetup();
        ISchedulerBuilder UseUserInformationFromMeetup();
        ISchedulerBuilder SetLogger(ILog logger);
        ISchedulerBuilder ExcludeGroup(string groupUrl);
        ISchedulerBuilder NotifyClientsWithCredentials(EmailConfiguration emailConfiguration);
        Task<IRsvpScheduler> Build(string userName);
        Task<IScheduler> GetScheduler();
    }
}