﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using MeetupApi.MeetupClient;
using MeetupApi.MeetupUri;
using MeetupApiDto.UserInformation;
using MeetupEmailSender.MeetupEmailSender;
using Quartz;
using Quartz.Impl;
using RsvpScheduler.Builder.Factories;
using RsvpScheduler.Proxy;
using RsvpScheduler.Scheduler;
using RsvpScheduler.Verifier;
using RsvpScheduler.Verifier.Builder;

namespace RsvpScheduler.Builder
{
    public sealed class SchedulerBuilder : ISchedulerBuilder
    {
        private readonly List<string> _groupsUrl = new List<string>();
        private readonly string _key;
        private readonly Task<IScheduler> _scheduler;
        private readonly IRsvpConfigurationVerifierBuilder _rsvpConfigurationVerifierBuilder;
        private readonly AuthenticationType _meetupAuthenticationType;

        private bool _dontSignForEventsInThePast;
        private string _email;
        private ILog _logger;
        private bool _signForFutureRsvps;
        private bool _useEmailFromMeetup;

        private UserDataDto _userData;
        private EmailConfiguration _emailConfiguration;

        private SchedulerBuilder(string key, string groupUrl, AuthenticationType authenticationType) : this(key, authenticationType)
        {
            _groupsUrl.Add(groupUrl);
        }

        private SchedulerBuilder(string key, AuthenticationType authenticationType)
        {
            _key = key;
            var schedulerFactory = new StdSchedulerFactory();
            _scheduler = schedulerFactory.GetScheduler();

            _rsvpConfigurationVerifierBuilder = new RsvpConfigurationVerifierBuilder(new List<IRsvpConfigurationVerifier>());
            _meetupAuthenticationType = authenticationType;
        }

        public static ISchedulerBuilder Create(string key, string groupUrl, AuthenticationType authenticationType)
        {
            return new SchedulerBuilder(key, groupUrl, authenticationType);
        }

        public static ISchedulerBuilder Create(string key, AuthenticationType authenticationType)
        {
            return new SchedulerBuilder(key, authenticationType);
        }

        public ISchedulerBuilder SignOnlyInCity(string city)
        {
            _userData.City = city;
            return this;
        }

        public ISchedulerBuilder SendInformationToEmail(string myEmail)
        {
            _email = myEmail.Trim();
            return this;
        }

        public ISchedulerBuilder SendInformationToEmailRetrievedFromMeetup()
        {
            _useEmailFromMeetup = true;
            return this;
        }

        public ISchedulerBuilder DontSignForEventsInThePast()
        {
            _dontSignForEventsInThePast = true;
            return this;
        }

        public ISchedulerBuilder AddGroupToRsvp(string groupUrl)
        {
            _groupsUrl.Add(groupUrl);
            return this;
        }

        public ISchedulerBuilder AddGroupToRsvp(IEnumerable<string> groupsUrl)
        {
            _groupsUrl.AddRange(groupsUrl);
            return this;
        }

        public ISchedulerBuilder SignForFutureRsvpInfinitely()
        {
            _signForFutureRsvps = true;
            return this;
        }

        public ISchedulerBuilder UseUserGroupsFromMeetup()
        {
            var client = GetMeetupClient();
            try
            {
                var groupsDto = client.GetGroupsThatUserBelongsTo().Result;
                foreach (var group in groupsDto)
                {
                    _groupsUrl.Add(group.GroupUrl);
                }
            }
            catch (AggregateException e)
            {
                var logger = GetLogger();
                logger.Error("MeetupClient error: " + e.Flatten().Message);
            }

            return this;
        }
        
        public ISchedulerBuilder UseUserCityFromMeetup()
        {
            var client = GetMeetupClient();
            try
            {
                _userData = client.GetUserData().Result;
            }
            catch (AggregateException e)
            {
                var logger = GetLogger();
                logger.Error("MeetupClient error: " + e.Flatten().Message);
            }

            return this;
        }

        public ISchedulerBuilder UseUserInformationFromMeetup()
        {
            UseUserCityFromMeetup();
            UseUserGroupsFromMeetup();
            return this;
        }

        public ISchedulerBuilder SetLogger(ILog logger)
        {
            _logger = logger;
            return this;
        }

        public ISchedulerBuilder ExcludeGroup(string groupUrl)
        {
            if (_groupsUrl.Remove(groupUrl))
            {
                return this;
            }

            _logger?.Warn("Group with this url was not added");
            throw new Exception("Group with this url was not added : " + groupUrl);
        }

        public ISchedulerBuilder NotifyClientsWithCredentials(EmailConfiguration emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
            return this;
        }

        public async Task<IRsvpScheduler> Build(string userName)
        {
            if (_groupsUrl.Count == 0)
            {
                _logger?.Error("Builder should have at least one group url");
                throw new ArgumentException("Builder should have at least one group url try UseUserGroupsFromMeetup() or AddGroupUrl");
            }
            var uniqueGroups = _groupsUrl.Distinct().ToList();

            _logger = GetLogger();

            var city = _userData?.City;

            if (_userData == null)
            {
                UseUserCityFromMeetup();
            }

            if (_useEmailFromMeetup)
            {
                _email = _userData?.Email;
            }

            var emailSender = EmailFactory.Create(_email, _logger, _emailConfiguration);

            if (!string.IsNullOrEmpty(_email))
            {
                var emailConfiguration = new EmailConfigurationMessage(
                    uniqueGroups,
                    city,
                    _dontSignForEventsInThePast,
                    _signForFutureRsvps);

                emailSender.SendWelcomeMailWithConfiguratiom(_userData, emailConfiguration);
            }

            var proxyConfiguration = new SchedulerProxyConfiguration(
                uniqueGroups,
                city,
                _dontSignForEventsInThePast,
                _signForFutureRsvps,
                userName);

            var scheduler = await _scheduler;
            await scheduler.Start();

            var rsvpSchedulerProxyBuilder = new RsvpSchedulerProxyBuilder(new List<IRsvpScheduler>());

            return rsvpSchedulerProxyBuilder.Build(
                GetMeetupClient(),
                proxyConfiguration,
                _rsvpConfigurationVerifierBuilder,
                scheduler,
                emailSender,
                _logger,
                new SchedulerKeyGenerator());
        }

        public async Task<IScheduler> GetScheduler()
        {
            return await _scheduler;
        }

        private ILog GetLogger()
        {
            return _logger ?? new NullLogger.NullLogger();
        }
        private MeetupClient GetMeetupClient()
        {
            var uriGenerator = new MeetupUriGenerator(_meetupAuthenticationType, _key);
            var client = new MeetupClient(GetLogger(), uriGenerator);
            return client;
        }
    }
}