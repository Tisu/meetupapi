﻿using System.Collections.Generic;
using System.Linq;
using MeetupApiDto.Event;

namespace RsvpScheduler.Verifier.Proxy
{
    public class RsvpConfigurationVerifierProxy : IRsvpConfigurationVerifier
    {
        private readonly List<IRsvpConfigurationVerifier> _verifiers;

        public RsvpConfigurationVerifierProxy(List<IRsvpConfigurationVerifier> verifiers)
        {
            _verifiers = verifiers;
        }

        public bool Verify(EventDto eventInformation)
        {
            return _verifiers.All(veryfier => veryfier.Verify(eventInformation));
        }
    }
}