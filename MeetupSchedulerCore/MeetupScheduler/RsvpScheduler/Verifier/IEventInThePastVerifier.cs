﻿using MeetupApiDto.Event;

namespace RsvpScheduler.Verifier
{
    public interface IEventInThePastVerifier
    {
        bool IsEventInThePast(EventDto eventDto);
    }
}