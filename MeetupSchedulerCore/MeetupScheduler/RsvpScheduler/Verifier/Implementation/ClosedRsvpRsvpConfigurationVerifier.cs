﻿using MeetupApiDto.Event;

namespace RsvpScheduler.Verifier.Implementation
{
    public class ClosedRsvpRsvpConfigurationVerifier : IRsvpConfigurationVerifier
    {
        public bool Verify(EventDto eventInformation)
        {
            return !eventInformation.RsvpRules.Closed;
        }
    }
}