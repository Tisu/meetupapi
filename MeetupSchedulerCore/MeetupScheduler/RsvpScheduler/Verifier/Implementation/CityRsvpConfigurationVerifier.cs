﻿using MeetupApiDto.Event;
using RsvpScheduler.Common;

namespace RsvpScheduler.Verifier.Implementation
{
    public class CityRsvpConfigurationVerifier : IRsvpConfigurationVerifier
    {
        private readonly string _city;

        public CityRsvpConfigurationVerifier(string city)
        {
            _city = city;
        }

        public bool Verify(EventDto eventInformation)
        {
            return eventInformation.Venue != null && eventInformation.Venue.City.CompareWithoutPolishLettersIgnoreCase(_city); ;
        }
    }
}