﻿using System;
using MeetupApiDto.Event;
using MeetupApiDto.Extender;

namespace RsvpScheduler.Verifier.Implementation
{
    public class EventInThePastRsvpConfigurationVerifier: IRsvpConfigurationVerifier, IEventInThePastVerifier
    {
        public bool Verify(EventDto eventInformation)
        {
            return eventInformation.RsvpRules.OpenTime.GetDateTimeFromUnixTimestampUtc() >= DateTime.UtcNow;
        }

        public bool IsEventInThePast(EventDto eventDto)
        {
            return !Verify(eventDto);
        }
    }
}