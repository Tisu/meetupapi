﻿using MeetupApiDto.Event;

namespace RsvpScheduler.Verifier
{
    public interface IRsvpConfigurationVerifier
    {
        bool Verify(EventDto eventInformation);
    }
}