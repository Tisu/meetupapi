﻿using RsvpScheduler.Common;

namespace RsvpScheduler.Verifier.Builder
{
    public interface IRsvpConfigurationVerifierBuilder
    {
        IRsvpConfigurationVerifier Build(SchedulerConfiguration schedulerConfiguration);
    }
}