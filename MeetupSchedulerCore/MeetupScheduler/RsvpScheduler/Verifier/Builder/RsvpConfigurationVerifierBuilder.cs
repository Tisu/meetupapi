﻿using System.Collections.Generic;
using RsvpScheduler.Common;
using RsvpScheduler.Verifier.Implementation;
using RsvpScheduler.Verifier.Proxy;

namespace RsvpScheduler.Verifier.Builder
{
    public class RsvpConfigurationVerifierBuilder : IRsvpConfigurationVerifierBuilder
    {
        private readonly List<IRsvpConfigurationVerifier> _verifiers;

        public RsvpConfigurationVerifierBuilder(List<IRsvpConfigurationVerifier> verifiers)
        {
            verifiers.Clear();

            _verifiers = verifiers;
            _verifiers.Add(new ClosedRsvpRsvpConfigurationVerifier());
        }
        
        public IRsvpConfigurationVerifier Build(SchedulerConfiguration schedulerConfiguration)
        {
            if (!string.IsNullOrEmpty(schedulerConfiguration.City))
            {
                _verifiers.Add(new CityRsvpConfigurationVerifier(schedulerConfiguration.City));
            }

            if (schedulerConfiguration.DontSignForEventsInThePast)
            {
                _verifiers.Add(new EventInThePastRsvpConfigurationVerifier());
            }

            return new RsvpConfigurationVerifierProxy(_verifiers);
        }
    }
}