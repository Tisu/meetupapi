﻿using System;
using MeetupApiDto.Event;
using Quartz;

namespace RsvpScheduler.Scheduler
{
    public class SchedulerKeyGenerator : ISchedulerKeyGenerator
    {
        private const char Separator = ':';
        public TriggerKey GenerateTriggerKey(DeputingType deputingModul, string eventName, string userName)
        {
            return new TriggerKey(GenerateKey(deputingModul.ToString(), userName, eventName), userName);
        }

        public TriggerKey GenerateTriggerKey(DeputingType deputingModul, string eventName, string userName, string eventId)
        {
            return new TriggerKey(GenerateKey(deputingModul.ToString(), eventId, userName , eventName), userName);
        }

        public JobKey GenerateJobKey(DeputingType deputingModul, string eventName, string userName)
        {
            return new JobKey(GenerateKey(deputingModul.ToString(), userName, eventName), userName);
        }
       
        private static string GenerateKey(params string[] parameters)
        {
            return string.Join(Separator, parameters);
        }
    }
}