﻿using System.Collections.Generic;
using System.Threading.Tasks;
using log4net;
using MeetupApi.MeetupClient;
using MeetupApiDto.Event;
using MeetupApiDto.Extender;
using MeetupEmailSender.MeetupEmailSender;
using MeetupEmailSender.Messages;
using Quartz;
using RsvpScheduler.Event;
using RsvpScheduler.Jobs;
using RsvpScheduler.QuartzKeys;
using RsvpScheduler.Verifier;

namespace RsvpScheduler.Scheduler
{
    internal sealed class MeetupRsvpScheduler : IRsvpScheduler
    {
        private readonly IScheduler _scheduler;
        private readonly IMeetupClient _meetupClient;
        private readonly string _groupUrl;
        private readonly IRsvpConfigurationVerifier _rsvpConfigurationVerifier;
        private readonly IMeetupEmailSender _emailSender;
        private readonly ILog _logger;
        private readonly IEventInThePastVerifier _eventInThePastVerifier;
        private readonly ISchedulerKeyGenerator _keyGenerator;
        private readonly string _userName;

        internal MeetupRsvpScheduler(
            IMeetupClient meetupClient,
            string groupUrl,
            IRsvpConfigurationVerifier rsvpConfigurationVerifier,
            IScheduler scheduler,
            IMeetupEmailSender emailSender,
            ILog logger,
            IEventInThePastVerifier eventInThePastVerifier,
            ISchedulerKeyGenerator keyGenerator,
            string userName)
        {
            _meetupClient = meetupClient;
            _groupUrl = groupUrl;
            _rsvpConfigurationVerifier = rsvpConfigurationVerifier;
            _scheduler = scheduler;
            _emailSender = emailSender;
            _logger = logger;
            _eventInThePastVerifier = eventInThePastVerifier;
            _keyGenerator = keyGenerator;
            _userName = userName;
        }

        ~MeetupRsvpScheduler()
        {
            Dispose();
        }
        public async Task ScheduleRsvpForNextMeeting()
        {
            var eventsDto = await _meetupClient.GetRsvpRulesAsync(_groupUrl);
            foreach (var eventDto in eventsDto)
            {
                if (await CheckIfJobAlreadyExists(eventDto, _userName))
                {
                    _logger.Debug($"Job with name {_keyGenerator.GenerateJobKey(DeputingType.Event, eventDto.EventId, _userName)} exists skipping");
                    continue;
                }

                if (!_rsvpConfigurationVerifier.Verify(eventDto))
                {
                    _logger.Warn($"Event {eventDto.Name} skipped due to configuration missmatch.");
                    continue;
                }

                if (_eventInThePastVerifier.IsEventInThePast(eventDto))
                {
                    _emailSender.SendScheduledRsvpAsync(eventDto);
                }

                ScheduleJob(eventDto, _userName);
            }
        }

        private async Task<bool> CheckIfJobAlreadyExists(EventDto eventDto, string userName)
        {
            return await _scheduler.CheckExists(GenerateJobKey(eventDto, userName));
        }

        private JobKey GenerateJobKey(EventDto eventDto, string userName)
        {
            return _keyGenerator.GenerateJobKey(DeputingType.Event, eventDto.EventId, userName);
        }

        private void ScheduleJob(EventDto eventDto, string userName)
        {
            var dict = CreateJobData(eventDto);
            _logger.Debug($"Scheduling rsvp for event {eventDto.Name} ");
            var jobData = new JobDataMap((IDictionary<string, object>)dict);

            var job = JobBuilder.Create<MeetupRsvpJob>()
                .WithIdentity(GenerateJobKey(eventDto, userName))
                .SetJobData(jobData)
                .Build();

            var trigger = TriggerBuilder.Create()
                .WithIdentity(_keyGenerator.GenerateTriggerKey(DeputingType.Event, eventDto.Group.GroupName, userName, eventDto.EventId))
                .StartAt(eventDto.RsvpRules.OpenTime.GetDateTimeFromUnixTimestampUtc())
                .WithSimpleSchedule(x => x.WithMisfireHandlingInstructionNowWithRemainingCount())
                .UsingJobData(jobData)
                .ForJob(job)
                .Build() as ISimpleTrigger;

            _scheduler.ScheduleJob(job, trigger);
        }

        private Dictionary<string, object> CreateJobData(EventDto eventDto)
        {
            var eventInformation = new EventInformation
            {
                DeputingModule = DeputingType.NewEventChecker,
                EventName = eventDto.Name,
                GroupName = eventDto.Group?.GroupName,
                GroupUrl = eventDto.Group?.GroupUrl
            };

            return new Dictionary<string, object>()
            {
                [MeetupJobKeys.EventId] = eventDto.EventId.ToString(),
                [MeetupJobKeys.MeetupClient] = _meetupClient,
                [MeetupJobKeys.SuccessfullMessage] =
                new SuccessfullRsvpMessage(eventDto.Link, eventDto.Name, _emailSender,
                    eventDto.EventStartTime.GetDateTimeFromUnixTimestamp(eventDto.UtcOffset)),
                [MeetupJobKeys.Logger] = _logger,
                [MeetupJobKeys.EventDetails] = eventInformation
            };
        }

        public void Dispose()
        {
            _meetupClient.Dispose();
        }
    }
}
