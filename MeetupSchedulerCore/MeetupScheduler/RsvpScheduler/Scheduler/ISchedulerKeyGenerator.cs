﻿using MeetupApiDto.Event;
using Quartz;

namespace RsvpScheduler.Scheduler
{
    public interface ISchedulerKeyGenerator
    {
        JobKey GenerateJobKey(DeputingType deputingModul, string eventName, string userName);
        TriggerKey GenerateTriggerKey(DeputingType deputingModul, string eventId, string userName);
        TriggerKey GenerateTriggerKey(DeputingType deputingModul, string eventName, string userName, string eventId);
    }
}