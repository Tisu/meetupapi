﻿using System;
using System.Threading.Tasks;

namespace RsvpScheduler.Scheduler
{
    public interface IRsvpScheduler : IDisposable
    {
        Task ScheduleRsvpForNextMeeting();
    }
}