﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using log4net;
using Quartz;
using RsvpScheduler.Event;
using RsvpScheduler.Jobs;
using RsvpScheduler.QuartzKeys;

namespace RsvpScheduler.Scheduler
{
    public sealed class NewEventsCheckerScheduler : IRsvpScheduler
    {
        private readonly IRsvpScheduler _meetupRsvpScheduler;
        private readonly IScheduler _scheduler;
        private readonly TriggerKey _triggerKey;
        private readonly ILog _logger;
        private readonly EventInformation _eventInformation;

        internal NewEventsCheckerScheduler(
            IRsvpScheduler meetupRsvpMeetupRsvpScheduler,
            IScheduler scheduler,
            TriggerKey triggerKey,
            ILog logger,
            EventInformation eventInformation)
        {
            _meetupRsvpScheduler = meetupRsvpMeetupRsvpScheduler;
            _scheduler = scheduler;
            _triggerKey = triggerKey;
            _logger = logger;
            _eventInformation = eventInformation;
        }
        public void Dispose()
        {
        }

        public async Task ScheduleRsvpForNextMeeting()
        {
            _logger.Debug($"Checking for new events {_triggerKey.Group} everyday at {DateTime.UtcNow:T} utc time");

            if (await CheckIfJobAlreadyExists())
            {
                return;
            }
            var jobData = CreateJobData();

            var job = JobBuilder.Create<MeetupNewEventCheckerJob>()
                .WithIdentity(_triggerKey.Name, _triggerKey.Group)
                .UsingJobData(jobData)
                .Build();

            var schedule = SimpleScheduleBuilder.RepeatHourlyForTotalCount(int.MaxValue, 24);

            var trigger = TriggerBuilder.Create()
                .WithIdentity(_triggerKey)
                .StartAt(DateBuilder.TomorrowAt(DateTime.UtcNow.Hour, DateTime.UtcNow.Minute, DateTime.UtcNow.Second))
                .WithSchedule(schedule)
                .UsingJobData(jobData)
                .ForJob(job).Build() as ISimpleTrigger;


            await _scheduler.ScheduleJob(job, trigger);
        }
        private JobDataMap CreateJobData()
        {
            return new JobDataMap((IDictionary<string, object>)new Dictionary<string, object>()
            {
                [MeetupJobKeys.RsvpScheduler] = _meetupRsvpScheduler,
                [MeetupJobKeys.Logger] = _logger,
                [MeetupJobKeys.EventDetails] = _eventInformation
            });
        }
        private async Task<bool> CheckIfJobAlreadyExists()
        {
            return await _scheduler.CheckExists(_triggerKey);
        }
    }
}