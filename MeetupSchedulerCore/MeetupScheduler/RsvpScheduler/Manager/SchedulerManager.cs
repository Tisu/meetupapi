﻿using System.Collections.Concurrent;
using System.Threading.Tasks;
using Quartz;
using Quartz.Impl.Matchers;

namespace RsvpScheduler.Manager
{
    public class SchedulerManager : ISchedulerManager
    {
        private readonly ConcurrentDictionary<string, IScheduler> _usersScheduler = new ConcurrentDictionary<string, IScheduler>();
        
        public void AddScheduler(string userName, IScheduler scheduler)
        {
            _usersScheduler.TryAdd(userName, scheduler);
        }

        public IScheduler GetScheduler(string username)
        {
            _usersScheduler.TryGetValue(username, out var scheduler);
            return scheduler;
        }

        public async Task<bool> RemoveAllJobsForUser(string userName)
        {
            var scheduler = GetScheduler(userName);
            if (scheduler == null)
            {
                return false;
            }

            var jobKeys = await scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(userName));
            return await scheduler.DeleteJobs(jobKeys);
        }
    }
}