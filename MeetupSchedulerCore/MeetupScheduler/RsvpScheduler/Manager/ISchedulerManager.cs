﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Quartz;

namespace RsvpScheduler.Manager
{
    public interface ISchedulerManager
    {
        void AddScheduler(string userName, IScheduler scheduler);
        IScheduler GetScheduler(string username);
        Task<bool> RemoveAllJobsForUser(string userName);
    }
}