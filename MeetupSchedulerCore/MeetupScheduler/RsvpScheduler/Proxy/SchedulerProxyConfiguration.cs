﻿using System.Collections.Generic;

namespace RsvpScheduler.Proxy
{
    public struct SchedulerProxyConfiguration
    {
        public bool SignForEventsInFuture { get; set; }
        public bool DontSignForEventsInThePast { get; set; }
        public List<string> GroupUrl { get; set ; }
        public string City { get; set; }
        public string UserName { get; set; }
       
        public SchedulerProxyConfiguration(
            List<string> groupUrl, 
            string city,
            bool dontSignForEventsInThePast,
            bool signForEventsInFuture,
            string userName)
        {
            SignForEventsInFuture = signForEventsInFuture;
            DontSignForEventsInThePast = dontSignForEventsInThePast;
            GroupUrl = groupUrl;
            City = city;
            UserName = userName;
        }
    }
}