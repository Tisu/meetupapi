﻿using System.Collections.Generic;
using log4net;
using MeetupApi.MeetupClient;
using MeetupEmailSender.MeetupEmailSender;
using Quartz;
using RsvpScheduler.Common;
using RsvpScheduler.Event;
using RsvpScheduler.Scheduler;
using RsvpScheduler.Verifier.Builder;
using RsvpScheduler.Verifier.Implementation;

namespace RsvpScheduler.Proxy
{
    public class RsvpSchedulerProxyBuilder
    {
        private readonly List<IRsvpScheduler> _rsvpSchedulers;

        public RsvpSchedulerProxyBuilder(List<IRsvpScheduler> rsvpSchedulers)
        {
            _rsvpSchedulers = rsvpSchedulers;
        }

        public IRsvpScheduler Build(
            IMeetupClient meetupClient,
            SchedulerProxyConfiguration configuration,
            IRsvpConfigurationVerifierBuilder rsvpConfigurationVerifierBuilder,
            IScheduler scheduler,
            IMeetupEmailSender emailSender,
            ILog logger,
            ISchedulerKeyGenerator schedulerKeyGenerator)
        {
            foreach (var group in configuration.GroupUrl)
            {
                if (configuration.SignForEventsInFuture)
                {
                    var schedulerConfigurationForFutureEvents = new SchedulerConfiguration(configuration.City, true);
                    var meetupRsvpScheduler = new MeetupRsvpScheduler(
                        meetupClient,
                        group,
                        rsvpConfigurationVerifierBuilder.Build(schedulerConfigurationForFutureEvents),
                        scheduler,
                        emailSender,
                        logger,
                        new EventInThePastRsvpConfigurationVerifier(),
                        schedulerKeyGenerator,
                        configuration.UserName);

                    var key = schedulerKeyGenerator.GenerateTriggerKey(DeputingType.NewEventChecker, group, configuration.UserName);
                    var eventInformation = new EventInformation
                    {
                        DeputingModule = DeputingType.NewEventChecker,
                        GroupUrl = group
                    };

                    _rsvpSchedulers.Add(
                        new NewEventsCheckerScheduler(
                            meetupRsvpScheduler,
                            scheduler,
                            key,
                            logger,
                            eventInformation));
                }

                var schedulerConfiguration = new SchedulerConfiguration(configuration.City, configuration.DontSignForEventsInThePast);

                _rsvpSchedulers.Add(new MeetupRsvpScheduler(
                    meetupClient,
                    group,
                    rsvpConfigurationVerifierBuilder.Build(schedulerConfiguration),
                    scheduler,
                    emailSender,
                    logger,
                    new EventInThePastRsvpConfigurationVerifier(),
                    new SchedulerKeyGenerator(),
                    configuration.UserName));

            }
            return new RsvpSchedulerProxy(scheduler, _rsvpSchedulers);
        }
    }
}