﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Quartz;
using RsvpScheduler.Scheduler;

[assembly: InternalsVisibleTo("RsvpScheduler.Tests")]
namespace RsvpScheduler.Proxy{
    
    public sealed class RsvpSchedulerProxy : IRsvpScheduler
    {
        private readonly IScheduler _scheduler;
        private readonly List<IRsvpScheduler> _rsvpSchedulers;

        internal RsvpSchedulerProxy(IScheduler scheduler, List<IRsvpScheduler> rsvpSchedulers)
        {
            _scheduler = scheduler;
            _rsvpSchedulers = rsvpSchedulers;
        }
        public void Dispose()
        {
            _rsvpSchedulers.ForEach(x => x.Dispose());
            _scheduler.Shutdown();
            _rsvpSchedulers.Clear();
        }

        public async Task ScheduleRsvpForNextMeeting()
        {
            foreach(var rsvpScheduler in _rsvpSchedulers)
            {
               await rsvpScheduler.ScheduleRsvpForNextMeeting();
            }
        }
    }
}