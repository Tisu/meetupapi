﻿using RsvpScheduler.Scheduler;

namespace RsvpScheduler.Event
{
    public class EventInformation
    {
        public string GroupUrl { get; set; }

        public string GroupName { get; set; }

        public string EventName { get; set; }

        public DeputingType DeputingModule { get; set; }
    }
}