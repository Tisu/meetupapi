﻿using System.Net;
using System.Net.Mail;

namespace RsvpScheduler
{
    public class EmailConfiguration
    {
        public EmailConfiguration(MailAddress email, NetworkCredential credential)
        {
            Email = email;
            Credential = credential;
        }

        public MailAddress Email { get; }
        public NetworkCredential Credential { get; }
    }
}