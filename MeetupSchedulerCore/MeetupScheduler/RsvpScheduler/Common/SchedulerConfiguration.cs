﻿namespace RsvpScheduler.Common
{
    public struct SchedulerConfiguration
    {
        public bool DontSignForEventsInThePast { get; }
        public string City { get; }

        public SchedulerConfiguration(string city, bool dontSignForEventsInThePast)
        {
            DontSignForEventsInThePast = dontSignForEventsInThePast;
            City = city;
        }
    }
}