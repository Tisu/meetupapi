﻿using System;
using System.Text;

namespace RsvpScheduler.Common
{
    public static class StringExtender
    {
        public static string RemovePolishLetter(string word)
        {
            var builder = new StringBuilder();

            foreach (var letter in word)
            {
                switch (letter)
                {
                    case 'ą':
                        builder.Append('a');
                        break;
                    case 'ę':
                        builder.Append('e');
                        break;
                    case 'ć':
                        builder.Append('c');
                        break;
                    case 'ź':
                        builder.Append('z');
                        break;
                    case 'ó':
                        builder.Append('o');
                        break;
                    case 'ł':
                        builder.Append('l');
                        break;
                    case 'ś':
                        builder.Append('s');
                        break;
                    case 'ż':
                        builder.Append('z');
                        break;
                    case 'ń':
                        builder.Append('n');
                        break;
                    default:
                        builder.Append(letter);
                        break;
                }
            }

            return builder.ToString();
        }

        public static bool CompareWithoutPolishLettersIgnoreCase(this string word, string compareTo)
        {
            return string.Equals(RemovePolishLetter(word), RemovePolishLetter(compareTo), StringComparison.OrdinalIgnoreCase);
        }
    }
}