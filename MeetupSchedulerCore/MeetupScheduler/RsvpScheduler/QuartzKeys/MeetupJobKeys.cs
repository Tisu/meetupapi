﻿namespace RsvpScheduler.QuartzKeys
{
    public static class MeetupJobKeys
    {
        public const string EventId = "EventId";
        public const string MeetupClient = "MeetupClient";
        public const string SuccessfullMessage = "SuccessfulMessage";

        public const string RsvpScheduler = "RsvpScheduler";
        public const string Logger = "Logger";
        public const string UserName = "UserName";
        public const string EventDetails = "EventDetails";
    }
}