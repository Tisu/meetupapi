﻿using System;
using MeetupEmailSender.MeetupEmailSender;

namespace MeetupEmailSender.Messages
{
    public sealed class SuccessfullRsvpMessage : ISuccessfullRsvpMessage
    {
        private readonly string _link;
        private readonly string _name;
        private readonly DateTime _eventStartDateTime;
        private readonly IMeetupEmailSender _meetupEmailSender;

        public SuccessfullRsvpMessage(
            string link,
            string name,
            IMeetupEmailSender meetupEmailSender,
            DateTime eventStartDateTime)
        {
            _link = link;
            _name = name;
            _meetupEmailSender = meetupEmailSender;
            _eventStartDateTime = eventStartDateTime;
        }

        public void SendSuccessfulRsvpNotification()
        {
            _meetupEmailSender.SendScheduledRsvpAsync(_link, _name, _eventStartDateTime);
        }
    }
}