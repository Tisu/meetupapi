﻿namespace MeetupEmailSender.Messages
{
    public interface ISuccessfullRsvpMessage
    {
        void SendSuccessfulRsvpNotification();
    }
}