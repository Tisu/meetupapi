﻿using System.Text;

namespace MeetupEmailSender.StringBuilderExtender
{
    public static class StringBuilderExtender
    {
        public static void AppendNewLineXml(this StringBuilder builder)
        {
            builder.Append("<br>");
        }
    }
}