﻿using System;
using MeetupApiDto.Event;
using MeetupApiDto.UserInformation;

namespace MeetupEmailSender.MeetupEmailSender
{
    public interface IMeetupEmailSender
    {
        void SendScheduledRsvpAsync(EventDto eventDto);
        void SendScheduledRsvpAsync(string link, string name,DateTime eventStartTimeLocal);
        void SendWelcomeMailWithConfiguratiom(UserDataDto userData, EmailConfigurationMessage emailConfigurationMessage);
    }
}