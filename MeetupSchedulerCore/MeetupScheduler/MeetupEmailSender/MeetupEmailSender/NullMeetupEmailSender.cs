﻿using System;
using MeetupApiDto.Event;
using MeetupApiDto.UserInformation;

namespace MeetupEmailSender.MeetupEmailSender
{
    public class NullMeetupEmailSender : IMeetupEmailSender
    {
        public void SendScheduledRsvpAsync(EventDto eventDto)
        {
        }

        public void SendScheduledRsvpAsync(string link, string name, DateTime eventStartTimeLocal)
        {
        }

        public void SendWelcomeMailWithConfiguratiom(UserDataDto userData, EmailConfigurationMessage emailConfigurationMessage)
        {
        }
    }
}