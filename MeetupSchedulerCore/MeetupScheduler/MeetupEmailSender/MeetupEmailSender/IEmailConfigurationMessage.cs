﻿namespace MeetupEmailSender.MeetupEmailSender
{
    public interface IEmailConfigurationMessage
    {
        string ToEmailMessageXml();
    }
}