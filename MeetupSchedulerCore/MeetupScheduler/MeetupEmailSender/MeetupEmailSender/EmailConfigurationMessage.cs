﻿using System.Collections.Generic;
using System.Text;
using MeetupEmailSender.StringBuilderExtender;

namespace MeetupEmailSender.MeetupEmailSender
{
    public sealed class EmailConfigurationMessage : IEmailConfigurationMessage
    {
        public bool SignForEventsInFuture { get; set; }
        public bool DontSignForEventsInThePast { get; set; }
        public List<string> GroupUrl { get; set; }
        public string City { get; set; }

        public EmailConfigurationMessage(
           List<string> groupUrl,
           string city,
           bool dontSignForEventsInThePast,
           bool signForEventsInFuture)
        {
            SignForEventsInFuture = signForEventsInFuture;
            DontSignForEventsInThePast = dontSignForEventsInThePast;
            GroupUrl = groupUrl;
            City = city;
        }

        public string ToEmailMessageXml()
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.Append("Your configuration: ");
            stringBuilder.AppendNewLineXml();
            stringBuilder.Append("Groups that will be monitored: ");
            stringBuilder.AppendNewLineXml();
            foreach(var group in GroupUrl)
            {
                stringBuilder.Append($"\t - {group}");
                stringBuilder.AppendNewLineXml();
            }

            stringBuilder.AppendNewLineXml();
            var isEnabled = DontSignForEventsInThePast ?  " NOT " : " ";
            stringBuilder.Append($"Scheduler will {isEnabled} sign for events in the past");

            stringBuilder.AppendNewLineXml();
            isEnabled = SignForEventsInFuture ? " " : " NOT ";
            stringBuilder.Append($"Scheduler will {isEnabled} monitor future events for this groups.");

            stringBuilder.AppendNewLineXml();
            var city = string.IsNullOrEmpty(City) ? "You will be sign for events in every city" : $"You will be sign only for events in city: {City}";
            stringBuilder.Append(city);

            return stringBuilder.ToString();
        }
    }
}