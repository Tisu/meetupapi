﻿using System;
using System.Text;
using EmailSender.Sender;
using MeetupApiDto.Event;
using MeetupApiDto.Extender;
using MeetupApiDto.UserInformation;
using MeetupEmailSender.StringBuilderExtender;
using MeetupEmailSender.Titles;

namespace MeetupEmailSender.MeetupEmailSender
{
    public sealed class MeetupEmailSender : IMeetupEmailSender
    {
        private readonly IEmailSender _emailSender;

        public MeetupEmailSender(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        public void SendScheduledRsvpAsync(EventDto eventDto)
        {
            var stringBuilder= new StringBuilder();

            stringBuilder.Append("RSVP is opened at: ");
            stringBuilder.Append(eventDto.RsvpRules.OpenTime.GetDateTimeFromUnixTimestamp(eventDto.UtcOffset));
            stringBuilder.Append(" local time.");
            stringBuilder.AppendNewLineXml();

            stringBuilder.Append("Meeting starts at ");
            stringBuilder.Append(GetDateTimeFromUnixTimestamp(eventDto.EventStartTime, eventDto.UtcOffset).ToString());
            stringBuilder.Append(" in city: ");
            stringBuilder.Append(eventDto.Venue.City);

            stringBuilder.AppendNewLineXml();

            stringBuilder.Append("Link at meetup " + eventDto.Link);

            stringBuilder.AppendNewLineXml();

            stringBuilder.Append("<h2> Content of the meeting: </h2>");

            stringBuilder.Append(eventDto.Description);

            
            _emailSender.SendAsync(stringBuilder.ToString(), EmailTitles.GetRsvpScheduledTitle(eventDto.Name));
        }

        public void SendScheduledRsvpAsync(string link, string name,DateTime eventStartTimeLocal)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("You have been successful sign for ");
            stringBuilder.Append(name);
            stringBuilder.AppendNewLineXml();

            stringBuilder.Append("Event starts at ");
            stringBuilder.Append(eventStartTimeLocal.ToString());
            stringBuilder.Append(" local time");


            stringBuilder.AppendNewLineXml();
            stringBuilder.Append("Link at meetup: ");
            stringBuilder.Append(link);
            _emailSender.SendAsync(stringBuilder.ToString(), EmailTitles.GetRsvpSuccessfullTitle(name));
        }

        public void SendWelcomeMailWithConfiguratiom(UserDataDto userData, EmailConfigurationMessage emailConfigurationMessage)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.Append($"Hello  {userData.UserName},");
            stringBuilder.AppendNewLineXml();

            stringBuilder.Append("Thank you for using Rsvp scheduler :).");
            stringBuilder.AppendNewLineXml();
            stringBuilder.AppendNewLineXml();

            stringBuilder.Append(emailConfigurationMessage.ToEmailMessageXml());
            stringBuilder.AppendNewLineXml();
            stringBuilder.Append("You will be informed about scheduled rsvp and after successful process.");
            stringBuilder.AppendNewLineXml();
            stringBuilder.Append("Scheduler will check for new events once a day.");
            stringBuilder.AppendNewLineXml();
            stringBuilder.Append("Stay tuned!");
            _emailSender.SendAsync(stringBuilder.ToString(), EmailTitles.GetRsvpWelcome());
        }
        public DateTime GetDateTimeFromUnixTimestamp(long timestamp, long utcOffset)
        {
            var timeZone = TimeSpan.FromMilliseconds(utcOffset);
            return DateTimeOffset.FromUnixTimeMilliseconds(timestamp).DateTime + timeZone;
        }
    }
}