﻿namespace MeetupEmailSender.Titles
{
    public static class EmailTitles
    {
        public const string RsvpScheduled = "Successful scheduled rsvp for ";
        public const string RsvpSuccessful = "Successful signed for ";
        public const string RsvpWelcome = "Welcome!";

        public static string GetRsvpScheduledTitle(string groupName)
        {
            return RsvpScheduled + groupName;
        }
        public static string GetRsvpSuccessfullTitle(string groupName)
        {
            return RsvpSuccessful + groupName;
        }
        public static string GetRsvpWelcome()
        {
            return RsvpWelcome;
        }
    }
}