﻿using System.Threading.Tasks;

namespace EmailSender.Sender
{
    public interface IEmailSender
    {
        Task SendAsync(string message, string title);
    }
}