﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using log4net;

namespace EmailSender.Sender
{
    public class EmailSender : IEmailSender
    {
        private readonly MailAddress _emailReceiver;
        private readonly ILog _log;
        private readonly MailAddress _emailSender;
        private readonly NetworkCredential _credential;

        public EmailSender(MailAddress emailReceiver, MailAddress emailSender, NetworkCredential credential, ILog log)
        {
            _emailReceiver = emailReceiver;
            _log = log;
            _emailSender = emailSender;
            _credential = credential;
        }

        public async Task SendAsync(string message, string title)
        {
            using(var client = new SmtpClient())
            {
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Timeout = 20000;
                client.Host = "smtp.gmail.com";
                client.Credentials = _credential;
                try
                {
                    using(var mail = new MailMessage(_emailSender, _emailReceiver))
                    {
                        mail.IsBodyHtml = true;
                        mail.Subject = title;
                        mail.Body = message;
                        await client.SendMailAsync(mail);
                    }
                }
                catch(Exception e)
                {
                    _log.Error("Email sanding error: " + e.Message);
                }
            }
        }
    }
}
