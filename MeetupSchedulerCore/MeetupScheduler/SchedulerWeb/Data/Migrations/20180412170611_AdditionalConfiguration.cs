﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MeetupScheduler.Web.Data.Migrations
{
    public partial class AdditionalConfiguration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AdditionalConfigurationId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MeetupAdditionalConfiguration",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DontSignForEventsInThePast = table.Column<bool>(nullable: false),
                    SendNotificationToEmail = table.Column<bool>(nullable: false),
                    SignForFutureRsvpInfinitely = table.Column<bool>(nullable: false),
                    SignOnlyInUserCity = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeetupAdditionalConfiguration", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_AdditionalConfigurationId",
                table: "AspNetUsers",
                column: "AdditionalConfigurationId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_MeetupAdditionalConfiguration_AdditionalConfigurationId",
                table: "AspNetUsers",
                column: "AdditionalConfigurationId",
                principalTable: "MeetupAdditionalConfiguration",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_MeetupAdditionalConfiguration_AdditionalConfigurationId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "MeetupAdditionalConfiguration");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_AdditionalConfigurationId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "AdditionalConfigurationId",
                table: "AspNetUsers");
        }
    }
}
