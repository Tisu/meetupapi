﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MeetupScheduler.Web.Models;
using SchedulerWeb.Database;
using SchedulerWeb.Dto.Configuration;

namespace SchedulerWeb.Repository
{
    public interface IMeetupUserGroupsRepository
    {
        Task<List<ApplicationUser>> GetUsers();
        Task<ApplicationUser> GetUser(string userName);
        Task SaveUserGroups(string userName, IEnumerable<MeetupGroupMonitoring> userGroups, AdditionalConfiguration additionalConfiguration);
    }
}