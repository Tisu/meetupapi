﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MeetupScheduler.Web.Data;
using MeetupScheduler.Web.Models;
using Microsoft.EntityFrameworkCore;
using SchedulerWeb.Database;
using SchedulerWeb.Dto.Configuration;

namespace SchedulerWeb.Repository
{
    public class MeetupUserGroupsRepository : IMeetupUserGroupsRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public MeetupUserGroupsRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<ApplicationUser>> GetUsers()
        {
            var userGroups = await _dbContext.Users
                                             .AsNoTracking()
                                             .Include(x => x.MeetupGroupMonitorings)
                                             .Include(x => x.AdditionalConfiguration)
                                             .ToListAsync();

            return userGroups ?? new List<ApplicationUser>();
        }

        public async Task<ApplicationUser> GetUser(string userName)
        {
            return await _dbContext.Users
                                   .AsNoTracking()
                                   .Include(x => x.MeetupGroupMonitorings)
                                   .Include(x => x.AdditionalConfiguration)
                                   .FirstOrDefaultAsync(x => x.UserName == userName);
        }

        public async Task SaveUserGroups(string userName,
                                         IEnumerable<MeetupGroupMonitoring> userGroups,
                                         AdditionalConfiguration additionalConfiguration)
        {
            var user = await _dbContext.Users
                                        .Include(x => x.MeetupGroupMonitorings)
                                        .Include(x => x.AdditionalConfiguration)
                                        .FirstOrDefaultAsync(x => x.UserName == userName);

            foreach (var meetupGroupMonitoring in userGroups)
            {
                meetupGroupMonitoring.User = user;
            }
            user.MeetupGroupMonitorings = userGroups;

            if (user.AdditionalConfiguration == null)
            {
                user.AdditionalConfiguration = new MeetupAdditionalConfiguration();
            }

            user.AdditionalConfiguration.DontSignForEventsInThePast = additionalConfiguration.DontSignForEventsInThePast;
            user.AdditionalConfiguration.SendNotificationToEmail = additionalConfiguration.SendNotificationToEmail;
            user.AdditionalConfiguration.SignOnlyInUserCity = additionalConfiguration.SignOnlyInUserCity;
            user.AdditionalConfiguration.SignForFutureRsvpInfinitely = additionalConfiguration.SignForFutureRsvpInfinitely;

            await _dbContext.SaveChangesAsync();
        }
    }
}