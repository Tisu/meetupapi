﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MeetupApi.MeetupClient;
using MeetupApi.MeetupUri;
using MeetupApiDto.Event;
using MeetupApiDto.GroupInformation;
using Polly;

namespace SchedulerWeb.MeetupApi
{
    public class MeetupClientDecorator : IMeetupClient
    {
        private readonly IAsyncPolicy _policy;
        private readonly IMeetupClient _meetupClient;
        private readonly IMeetupUriGenerator _meetupUriGenerator;

        public MeetupClientDecorator(IMeetupClient meetupClient, IAsyncPolicy policy, IMeetupUriGenerator meetupUriGenerator)
        {
            _meetupClient = meetupClient;
            _policy = policy;
            _meetupUriGenerator = meetupUriGenerator;
        }

        public void Dispose()
        {
            _meetupClient.Dispose();
        }

        public Task<List<EventDto>> GetRsvpRulesAsync(string groupName)
        {
            return _policy.ExecuteAsync((_) => _meetupClient.GetRsvpRulesAsync(groupName), new Context(_meetupUriGenerator.GetGetRsvpRulesUrl(groupName)));
        }

        public Task<List<GroupsDto>> GetGroupsThatUserBelongsTo()
        {
            return _policy.ExecuteAsync((_) => _meetupClient.GetGroupsThatUserBelongsTo(), new Context(_meetupUriGenerator.GetUserGroupsUrl()));
        }

        public Task<bool> PostRsvpAsync(string eventId)
        {
            return _meetupClient.PostRsvpAsync(eventId);
        }
    }
}