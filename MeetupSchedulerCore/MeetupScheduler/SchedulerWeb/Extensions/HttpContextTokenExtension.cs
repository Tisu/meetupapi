﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using SchedulerWeb.Authentication;

namespace SchedulerWeb.Extensions
{
    public static class HttpContextTokenExtension
    {
        public static Task<string> GetAccessTokenAsync(this HttpContext httpContext)
        {
            return httpContext.GetTokenAsync(MeetupAuthenticationConstants.AuthenticationScheme, ".Token.access_token");
        }
    }
}