﻿using System;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;

namespace SchedulerWeb.Authentication
{
    public static class MeetupAuthenticationExtension
    {
        public static AuthenticationBuilder AddMeetup(this AuthenticationBuilder builder)
        {
            return builder.AddMeetup(MeetupAuthenticationConstants.AuthenticationScheme, options => { });
        }

        public static AuthenticationBuilder AddMeetup(this AuthenticationBuilder builder, Action<MeetupOptions> configuration)
        {
            return builder.AddMeetup(MeetupAuthenticationConstants.AuthenticationScheme, configuration);
        }

        public static AuthenticationBuilder AddMeetup(this AuthenticationBuilder builder, string scheme, Action<MeetupOptions> configuration)
        {
            return builder.AddMeetup(scheme, MeetupAuthenticationConstants.DisplayName, configuration);
        }

        public static AuthenticationBuilder AddMeetup(
             this AuthenticationBuilder builder,
             string scheme,
             string caption,
             Action<MeetupOptions> configuration)
        {
            return builder.AddOAuth<MeetupOptions, MeetupHandler>(scheme, caption, configuration);
        }
    }
}