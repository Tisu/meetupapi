﻿using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Identity;

namespace SchedulerWeb.Authentication
{
    public class MeetupOptions : OAuthOptions
    {
        public MeetupOptions()
        {
            CallbackPath = MeetupAuthenticationConstants.CallbackPath;
            TokenEndpoint = MeetupAuthenticationConstants.TokenEndpoint;
            AuthorizationEndpoint = MeetupAuthenticationConstants.AuthorizationEndpoint;
            SaveTokens = true;
            SignInScheme = IdentityConstants.ExternalScheme;
            UserInformationEndpoint = MeetupAuthenticationConstants.UserInformationEndpoint;
            Scope.Add("basic");
            Scope.Add("ageless");
            Scope.Add("rsvp");
            ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "id");
            ClaimActions.MapJsonKey(ClaimTypes.Name, "name");
            ClaimActions.MapJsonKey(ClaimTypes.Surname, "last_name");
            ClaimActions.MapJsonKey(ClaimTypes.Email, "email");
        }
    }
}