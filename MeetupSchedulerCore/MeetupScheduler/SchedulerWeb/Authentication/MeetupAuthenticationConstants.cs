﻿namespace SchedulerWeb.Authentication
{
    public class MeetupAuthenticationConstants
    {
        public const string TokenEndpoint = "https://secure.meetup.com/oauth2/access";
        public const string AuthorizationEndpoint = "https://secure.meetup.com/oauth2/authorize";
        public const string UserInformationEndpoint = "https://api.meetup.com/2/member/self/";
        public const string AuthenticationScheme = "Meetup";
        public const string DisplayName = "Meetup";
        public const string CallbackPath = "/meetup-call-back-path";
    }
}