﻿using RsvpScheduler.Scheduler;

namespace SchedulerWeb.Dto.Monitoring
{
    public class ScheduleInformation
    {
        public string ScheduleTime { get; set; }

        public string GroupName { get; set; }

        public string DeputingType { get; set; }

        public string EventName { get; set; }
    }
}