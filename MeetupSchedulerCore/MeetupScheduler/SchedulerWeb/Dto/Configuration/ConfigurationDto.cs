﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SchedulerWeb.Dto.Configuration
{
    public class ConfigurationDto
    {
        [JsonProperty("groupsConfiguration")]
        public IEnumerable<GroupConfiguration> GroupConfiguration { get; set; }

        [JsonProperty("additionalConfiguration")]

        public AdditionalConfiguration AdditionalConfiguration { get; set; }
    }
}