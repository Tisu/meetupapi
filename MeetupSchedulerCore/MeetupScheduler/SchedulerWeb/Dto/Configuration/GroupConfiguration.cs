﻿using Newtonsoft.Json;

namespace SchedulerWeb.Dto.Configuration
{
    public class GroupConfiguration
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("monitoring")] 
        public bool Monitoring { get; set; }
    }
}