﻿using System.Collections.Generic;

namespace SchedulerWeb.Dto.Configuration.Comparer
{
    public sealed class GroupConfigurationEqualityComparer : IEqualityComparer<GroupConfiguration>
    {
        public bool Equals(GroupConfiguration x, GroupConfiguration y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null))
            {
                return false;
            }

            if (ReferenceEquals(y, null))
            {
                return false;
            }

            return string.Equals(x.Name, y.Name);
        }

        public int GetHashCode(GroupConfiguration obj)
        {
            return (obj.Name != null ? obj.Name.GetHashCode() : 0);
        }
    }
}