﻿using Newtonsoft.Json;

namespace SchedulerWeb.Dto.Configuration
{
    public class AdditionalConfiguration
    {
        [JsonProperty("dontSignForEventsInThePast")]
        public bool DontSignForEventsInThePast { get; set; }

        [JsonProperty("signForFutureRsvpInfinitely")]
        public bool SignForFutureRsvpInfinitely { get; set; }

        [JsonProperty("sendNotificationToEmail")]
        public bool SendNotificationToEmail { get; set; }

        [JsonProperty("signOnlyInUserCity")]
        public bool SignOnlyInUserCity { get; set; }
    }
}