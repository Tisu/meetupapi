﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeetupApi.MeetupClient;
using Microsoft.AspNetCore.Mvc;
using SchedulerWeb.Dto;
using SchedulerWeb.Dto.Configuration;
using SchedulerWeb.Managers.Configuration;

namespace SchedulerWeb.Controllers
{
    [Route("api/[controller]")]
    public class ConfigurationController : Controller
    {
        private readonly IConfigurationManager _configurationManager;

        public ConfigurationController(IConfigurationManager configurationManager)
        {
            _configurationManager = configurationManager;
        }

        [HttpGet("[action]")]
        public async Task<ConfigurationDto> GetUserMeetupGroups()
        {
           return await _configurationManager.GetGroupsThatUserBelongsTo(HttpContext.User.Identity.Name);
        }

        [HttpPost("[action]")]
        public async Task SaveConfiguration([FromBody] ConfigurationDto configurationDto)
        {
            await _configurationManager.ScheduleRsvpAsync(configurationDto, HttpContext.User.Identity.Name);
        }
    }
}