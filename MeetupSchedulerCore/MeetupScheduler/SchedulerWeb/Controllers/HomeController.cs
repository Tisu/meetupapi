﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace SchedulerWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAntiforgeryTokenProvider _antiforgeryTokenProvider;

        public HomeController(IAntiforgeryTokenProvider antiforgeryTokenProvider)
        {
            _antiforgeryTokenProvider = antiforgeryTokenProvider;
        }

        public IActionResult Index()
        {
            _antiforgeryTokenProvider.AddAntiForgeryToken(HttpContext);
            return View();
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}