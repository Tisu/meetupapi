﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Quartz;
using Quartz.Impl.Matchers;
using RsvpScheduler.Manager;
using SchedulerWeb.Dto.Monitoring;
using SchedulerWeb.Managers.Monitoring;

namespace SchedulerWeb.Controllers
{
    [Route("api/[controller]")]
    public class MonitoringController : Controller
    {
        private readonly IMonitoringManager _monitoringManager;

        public MonitoringController(IMonitoringManager monitoringManager)
        {
            _monitoringManager = monitoringManager;
        }

        [HttpGet("[action]")]
        public async Task<List<ScheduleInformation>> GetMonitoring()
        {
           return await _monitoringManager.GetSchedulesInformation(HttpContext.User.Identity.Name);
        }
    }
}