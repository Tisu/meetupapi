﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace MeetupScheduler.Web.Validation
{
    public class ValidateModelFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid || !(context.Controller is Controller controller))
            {
                base.OnActionExecuting(context);
                return;
            }
            
            context.Result = new ViewResult()
            {
                TempData = controller.TempData,
                ViewData = controller.ViewData
            };

            base.OnActionExecuting(context);
        }
    }
}