﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Configuration;

namespace SchedulerWeb.Crypto
{
    public class CipherService : ICipherService
    {
        public const string ConfigurationKey = "CryptoKey";
        private readonly IConfiguration _configuration;
        private readonly IDataProtectionProvider _dataProtectionProvider;

        public CipherService(IDataProtectionProvider dataProtectionProvider, IConfiguration configuration)
        {
            _dataProtectionProvider = dataProtectionProvider;
            _configuration = configuration;
        }

        public string Encrypt(string input)
        {
            var protector = _dataProtectionProvider.CreateProtector(_configuration[ConfigurationKey]);
            return protector.Protect(input);
        }

        public string Decrypt(string cipherText)
        {
            var protector = _dataProtectionProvider.CreateProtector(_configuration[ConfigurationKey]);
            return protector.Unprotect(cipherText);
        }
    }
}