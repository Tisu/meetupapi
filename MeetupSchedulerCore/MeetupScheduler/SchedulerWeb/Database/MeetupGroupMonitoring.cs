﻿using MeetupScheduler.Web.Models;

namespace SchedulerWeb.Database
{
    public class MeetupGroupMonitoring
    {
        public int Id { get; set; }
        public string GroupUrl { get; set; }

        public string GroupName { get; set; }

        public bool Monitoring { get; set; }

        public ApplicationUser User { get; set; }
    }
}