﻿namespace SchedulerWeb.Database
{
    public class MeetupAdditionalConfiguration
    {
        public int Id { get; set; }

        public bool DontSignForEventsInThePast { get; set; }

        public bool SignForFutureRsvpInfinitely { get; set; }

        public bool SendNotificationToEmail { get; set; }

        public bool SignOnlyInUserCity { get; set; }
    }
}