﻿import {AdditionalConfiguration} from "./AdditionalConfiguration";
import {GroupConfiguration} from "./GroupConfiguration";

export interface IConfiguration {
    additionalConfiguration: AdditionalConfiguration;
    groupsConfiguration: GroupConfiguration[];
}