﻿export class AdditionalConfiguration {
    dontSignForEventsInThePast: boolean;
    signForFutureRsvpInfinitely: boolean;
    sendNotificationToEmail: boolean;
    signOnlyInUserCity: boolean;

    constructor() {
        this.dontSignForEventsInThePast = true;
        this.signForFutureRsvpInfinitely = true;
        this.sendNotificationToEmail = true;
        this.signOnlyInUserCity = true;
    }
}