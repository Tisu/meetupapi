﻿import { Inject } from "@angular/core";
import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/Rx";
import {IConfiguration} from "./IConfiguration";

@Injectable()
export class ConfigurationService {

    private readonly http: Http;
    private readonly baseUrl: string;

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {
        this.http = http;
        this.baseUrl = baseUrl;
    }

    public getGroupConfiguration(): Observable<IConfiguration> {
        return this.http.get(this.baseUrl + 'api/Configuration/GetUserMeetupGroups').map((response: Response) => {
            return response.json() as IConfiguration;
        });
    }

    public saveConfiguration(configuration: IConfiguration): void {
        this.http.post(this.baseUrl + 'api/Configuration/SaveConfiguration', configuration).subscribe(result => {
        });
    }
}