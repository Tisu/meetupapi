﻿export class GroupConfiguration {
    name: string;
    monitoring: boolean;

    constructor(name: string, monitoring: boolean) {
        this.name = name;
        this.monitoring = monitoring;
    }
}