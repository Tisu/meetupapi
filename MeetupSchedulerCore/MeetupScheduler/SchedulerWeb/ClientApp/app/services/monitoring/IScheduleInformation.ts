﻿export interface IScheduleInformation {
    scheduleTime: string;
    groupName: string;
    deputingType: string;
    eventName:string;
}