﻿import { Inject } from "@angular/core";
import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/Rx";
import {IScheduleInformation} from "./IScheduleInformation";

@Injectable()
export class MonitoringService {

    private readonly http: Http;
    private readonly baseUrl: string;

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {
        this.http = http;
        this.baseUrl = baseUrl;
        console.log(baseUrl);
    }

    public getMonitoring(): Observable<IScheduleInformation[]> {
        return this.http.get(this.baseUrl + 'api/Monitoring/GetMonitoring').map((response: Response) => {
            console.log(response);
            return response.json() as IScheduleInformation[];
        });
    }
}
