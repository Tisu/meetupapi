import { Component } from '@angular/core';
import { MonitoringService } from "../../services/monitoring/MonitoringService";
import {IScheduleInformation} from "../../services/monitoring/IScheduleInformation";

@Component({
    selector: 'monitoring',
    templateUrl: './monitoring.component.html',
    providers: [MonitoringService]
})
export class MonitoringComponent {

    public  monitoringService: MonitoringService;
    public scheduleInformation: IScheduleInformation[];

    constructor(monitoringService: MonitoringService) {
        this.monitoringService = monitoringService;
        this.monitoringService.getMonitoring().subscribe(x => this.scheduleInformation = x);
    }
}
