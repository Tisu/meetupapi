﻿import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
    selector: '[configuration-row]',
    templateUrl: './configuration-row.component.html',
    styleUrls: ['./configuration-row.component.css']
})
export class ConfigurationRowComponent {
    @Input() public name: string;
    public initState: boolean = true;
    @Output() public state: EventEmitter<boolean> = new EventEmitter();

    @Input()
    set initialState(value: string) {
        this.initState = value !== 'false';
    }

    public monitoringClick(value: boolean): void {
        this.state.emit(value);
    }
}