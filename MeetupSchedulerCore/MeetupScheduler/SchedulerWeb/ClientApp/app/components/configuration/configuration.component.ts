﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { GroupConfiguration } from '../../services/configuration/GroupConfiguration';
import { AdditionalConfiguration } from '../../services/configuration/AdditionalConfiguration';
import { ConfigurationService } from "../../services/configuration/ConfigurationService";

@Component({
    selector: 'configuration',
    templateUrl: './configuration.component.html',
    styleUrls: ['./configuration.component.css'],
    providers: [ConfigurationService]
})
export class ConfigurationComponent {
    public readonly configurationService: ConfigurationService;

    public groupsConfiguration: GroupConfiguration[] = [];
    public additionalConfiguration: AdditionalConfiguration = new AdditionalConfiguration();

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string, configurationService: ConfigurationService) {
        this.configurationService = configurationService;

        this.configurationService.getGroupConfiguration().subscribe(userMeetupGroupsConfiguration => {

            this.groupsConfiguration = userMeetupGroupsConfiguration.groupsConfiguration;
            this.additionalConfiguration = userMeetupGroupsConfiguration.additionalConfiguration;
        }, error => console.error(error));
    }

    private groupMonitoringClick(groupName: string, value: boolean): void {
        let groupConfiguration: GroupConfiguration = this.groupsConfiguration.find(x => x.name === groupName) as GroupConfiguration;

        if (groupConfiguration == null) {
            return;
        }
        groupConfiguration.monitoring = value;
    }

    private saveConfiguration(): void {
        const configuration = {
            groupsConfiguration: this.groupsConfiguration,
            additionalConfiguration: this.additionalConfiguration
        }
        this.configurationService.saveConfiguration(configuration);
    }
}