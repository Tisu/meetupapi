﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SchedulerWeb.Dto.Monitoring;

namespace SchedulerWeb.Managers.Monitoring
{
    public interface IMonitoringManager
    {
        Task<List<ScheduleInformation>> GetSchedulesInformation(string userName);
    }
}