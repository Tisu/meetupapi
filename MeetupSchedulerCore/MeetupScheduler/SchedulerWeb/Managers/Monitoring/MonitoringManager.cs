﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeetupApi.MeetupClient;
using Quartz;
using Quartz.Impl.Matchers;
using RsvpScheduler.Event;
using RsvpScheduler.Manager;
using RsvpScheduler.QuartzKeys;
using SchedulerWeb.Dto.Monitoring;

namespace SchedulerWeb.Managers.Monitoring
{
    public class MonitoringManager : IMonitoringManager
    {
        private readonly ISchedulerManager _schedulerManager;
        private readonly IMeetupClient _meetupClient;

        public MonitoringManager(ISchedulerManager schedulerManager, IMeetupClient meetupClient)
        {
            _schedulerManager = schedulerManager;
            _meetupClient = meetupClient;
        }

        public async Task<List<ScheduleInformation>> GetSchedulesInformation(string userName)
        {
            var scheduler = _schedulerManager.GetScheduler(userName);
            if (scheduler == null)
            {
                return new List<ScheduleInformation>();
            }

            var allTriggerKeys = await scheduler.GetTriggerKeys(GroupMatcher<TriggerKey>.GroupEquals(userName));
            var result = new List<ScheduleInformation>();
            foreach (var triggerKey in allTriggerKeys)
            {
                var triggerDetails = await scheduler.GetTrigger(triggerKey);
                var eventInformation = triggerDetails.JobDataMap[MeetupJobKeys.EventDetails] as EventInformation;

                if (eventInformation.GroupName == null)
                {
                    var scheduleInformationFromMeetup = await GetInformationFromMeetup(eventInformation, triggerDetails);

                    result.Add(scheduleInformationFromMeetup);
                    continue;
                }

                var scheduleInformation = new ScheduleInformation()
                {
                    ScheduleTime = triggerDetails.GetNextFireTimeUtc().Value.ToLocalTime().ToString(),
                    GroupName = eventInformation.EventName ?? "Not applicable",
                    DeputingType = eventInformation.DeputingModule.ToString(),
                    EventName = eventInformation.EventName
                };
                result.Add(scheduleInformation);
            }

            return result;
        }

        private async Task<ScheduleInformation> GetInformationFromMeetup(EventInformation eventInformation, ITrigger triggerDetails)
        {
            var eventInformationFromMeetup = await _meetupClient.GetRsvpRulesAsync(eventInformation.GroupUrl);
            var groupInformation = eventInformationFromMeetup.FirstOrDefault(x => x.Group.GroupUrl == eventInformation.GroupUrl);
            var scheduleInformation = new ScheduleInformation()
            {
                ScheduleTime = triggerDetails.GetNextFireTimeUtc().Value.ToLocalTime().ToString(),
                GroupName = groupInformation.Group?.GroupName,
                DeputingType = eventInformation.DeputingModule.ToString(),
                EventName = groupInformation.Name,
            };

            return scheduleInformation;
        }
    }
}