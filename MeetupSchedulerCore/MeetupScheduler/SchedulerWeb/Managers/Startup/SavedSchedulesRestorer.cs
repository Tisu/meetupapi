﻿using System.Linq;
using System.Threading.Tasks;
using RsvpScheduler.Builder;
using SchedulerWeb.Repository;

namespace SchedulerWeb.Managers.Startup
{
    public class SavedSchedulesRestorer
    {
        private readonly IMeetupUserGroupsRepository _meetupUserGroupsRepository;
        private readonly ISchedulerBuilder _schedulerBuilder;

        public SavedSchedulesRestorer(IMeetupUserGroupsRepository meetupUserGroupsRepository, ISchedulerBuilder schedulerBuilder)
        {
            _meetupUserGroupsRepository = meetupUserGroupsRepository;
            _schedulerBuilder = schedulerBuilder;
        }

        public async Task RestoreForAllUsers()
        {
            var users = await _meetupUserGroupsRepository.GetUsers();

            foreach (var applicationUser in users)
            {
                var groupsToMonitor = applicationUser.MeetupGroupMonitorings.Where(x => x.Monitoring).Select(x => x.GroupUrl);
                ////var builder = SchedulerBuilder.Create(applicationUser.);
                ////_schedulerBuilder.AddGroupToRsvp(groupsToMonitor);
                ////_schedulerBuilder.
            }
        }
    }
}