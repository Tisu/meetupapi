﻿using System.Collections.Generic;
using SchedulerWeb.Dto;
using System.Threading.Tasks;
using SchedulerWeb.Dto.Configuration;

namespace SchedulerWeb.Managers.Configuration
{
    public interface IConfigurationManager
    {
        Task ScheduleRsvpAsync(ConfigurationDto configurationDto, string userName);
        Task<ConfigurationDto> GetGroupsThatUserBelongsTo(string userName);
    }
}