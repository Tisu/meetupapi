﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeetupApi.MeetupClient;
using MeetupApiDto.GroupInformation;
using MeetupScheduler.Web.Models;
using RsvpScheduler.Builder;
using RsvpScheduler.Manager;
using SchedulerWeb.Database;
using SchedulerWeb.Dto.Configuration;
using SchedulerWeb.Dto.Configuration.Comparer;
using SchedulerWeb.Repository;

namespace SchedulerWeb.Managers.Configuration
{
    public class ConfigurationManager : IConfigurationManager
    {
        private readonly ISchedulerBuilder _schedulerBuilder;
        private readonly ISchedulerManager _schedulerManager;
        private readonly IMeetupClient _meetupClient;
        private readonly IMeetupUserGroupsRepository _meetupUserGroupsRepository;

        public ConfigurationManager(
            ISchedulerBuilder schedulerBuilder,
            ISchedulerManager schedulerManager,
            IMeetupClient meetupClient,
            IMeetupUserGroupsRepository meetupUserGroupsRepository)
        {
            _schedulerBuilder = schedulerBuilder;
            _schedulerManager = schedulerManager;
            _meetupClient = meetupClient;
            _meetupUserGroupsRepository = meetupUserGroupsRepository;
        }

        public async Task<ConfigurationDto> GetGroupsThatUserBelongsTo(string userName)
        {
            var userSavedConfigurationTask = _meetupUserGroupsRepository.GetUser(userName);
            var meetupConfigurationTask = GetDefaultConfigurationFromMeetup();

            Task.WaitAll(userSavedConfigurationTask, meetupConfigurationTask);

            var userSavedConfiguration = await userSavedConfigurationTask;
            var meetupConfiguration = await meetupConfigurationTask;

            if (userSavedConfiguration != null && userSavedConfiguration.MeetupGroupMonitorings.Any())
            {
                var databaseConfiguration = GetConfigurationFromDatabase(userSavedConfiguration);

                databaseConfiguration.GroupConfiguration = databaseConfiguration.GroupConfiguration
                                                                                .Union(meetupConfiguration.GroupConfiguration, new GroupConfigurationEqualityComparer())
                                                                                .ToList();
                return databaseConfiguration;
            }

            return meetupConfiguration;
        }

        public async Task ScheduleRsvpAsync(ConfigurationDto configurationDto, string userName)
        {
            var userGroups = await _meetupClient.GetGroupsThatUserBelongsTo();
            var userMeetupGroups = MapToDatabaseEntity(configurationDto, userGroups);

            var saveUserGroupTask = _meetupUserGroupsRepository.SaveUserGroups(userName, userMeetupGroups, configurationDto.AdditionalConfiguration);

            await ScheduleEvents(configurationDto, userName, userGroups);
            await saveUserGroupTask;
        }

        private async Task<ConfigurationDto> GetDefaultConfigurationFromMeetup()
        {
            var configurationDto = new ConfigurationDto();
            var groupDto = await _meetupClient.GetGroupsThatUserBelongsTo();
            configurationDto.GroupConfiguration = groupDto.Select(x => new GroupConfiguration { Name = x.GroupName, Monitoring = true });
            GenerateDefaultAdditionalConfiguration(configurationDto);

            return configurationDto;
        }
        private static ConfigurationDto GetConfigurationFromDatabase(ApplicationUser userSavedConfiguration)
        {
            var configurationDtoFromDatabase = new ConfigurationDto
            {
                GroupConfiguration = userSavedConfiguration.MeetupGroupMonitorings.Select(x => new GroupConfiguration { Name = x.GroupName, Monitoring = x.Monitoring }).ToList(),
                AdditionalConfiguration = new AdditionalConfiguration()
                {
                    DontSignForEventsInThePast = userSavedConfiguration.AdditionalConfiguration?.DontSignForEventsInThePast ?? default,
                    SendNotificationToEmail = userSavedConfiguration.AdditionalConfiguration?.SendNotificationToEmail ?? default,
                    SignForFutureRsvpInfinitely = userSavedConfiguration.AdditionalConfiguration?.SignForFutureRsvpInfinitely ?? default,
                    SignOnlyInUserCity = userSavedConfiguration.AdditionalConfiguration?.SignOnlyInUserCity ?? default
                }
            };
            return configurationDtoFromDatabase;
        }

        private static void GenerateDefaultAdditionalConfiguration(ConfigurationDto configurationDto)
        {
            configurationDto.AdditionalConfiguration = new AdditionalConfiguration
            {
                DontSignForEventsInThePast = true,
                SendNotificationToEmail = true,
                SignForFutureRsvpInfinitely = true,
                SignOnlyInUserCity = true
            };
        }

        private async Task ScheduleEvents(ConfigurationDto configurationDto, string userName, List<GroupsDto> userGroups)
        {
            var wasJobsRemoved = await _schedulerManager.RemoveAllJobsForUser(userName);

            var groupsToMonitor = configurationDto.GroupConfiguration
                                                  .Where(x => x.Monitoring)
                                                  .Join(userGroups, x => x.Name, y => y.GroupName,(x, y) => y.GroupUrl)
                                                  .ToList();

            if (!groupsToMonitor.Any())
            {
                return;
            }

            _schedulerBuilder.AddGroupToRsvp(groupsToMonitor);

            if (configurationDto.AdditionalConfiguration.DontSignForEventsInThePast)
            {
                _schedulerBuilder.DontSignForEventsInThePast();
            }

            if (configurationDto.AdditionalConfiguration.SendNotificationToEmail)
            {
                _schedulerBuilder.SendInformationToEmailRetrievedFromMeetup();
            }

            if (configurationDto.AdditionalConfiguration.SignForFutureRsvpInfinitely)
            {
                _schedulerBuilder.SignForFutureRsvpInfinitely();
            }

            if (configurationDto.AdditionalConfiguration.SignOnlyInUserCity)
            {
                _schedulerBuilder.UseUserCityFromMeetup();
            }

            var rsvpScheduler = await _schedulerBuilder.Build(userName);
            var scheduler = await _schedulerBuilder.GetScheduler();

            _schedulerManager.AddScheduler(userName, scheduler);

            await rsvpScheduler.ScheduleRsvpForNextMeeting();
        }

        private static List<MeetupGroupMonitoring> MapToDatabaseEntity(ConfigurationDto configurationDto, List<GroupsDto> userGroups)
        {
            return userGroups.GroupJoin(
                configurationDto.GroupConfiguration,
                x => x.GroupName,
                y => y.Name, (dto, groupConfiguration) => new MeetupGroupMonitoring()
                {
                    GroupUrl = dto.GroupUrl,
                    GroupName = dto.GroupName,
                    Monitoring = groupConfiguration.Single().Monitoring
                }).ToList();
        }
    }
}