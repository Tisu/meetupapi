using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using log4net;
using MeetupApi.MeetupClient;
using MeetupApi.MeetupUri;
using MeetupScheduler.Web.Data;
using MeetupScheduler.Web.Models;
using MeetupScheduler.Web.Services;
using MeetupScheduler.Web.Validation;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations.Internal;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Caching;
using Polly.Caching.MemoryCache;
using Polly.Wrap;
using RsvpScheduler;
using RsvpScheduler.Builder;
using RsvpScheduler.Manager;
using RsvpScheduler.Scheduler;
using SchedulerWeb.Authentication;
using SchedulerWeb.Crypto;
using SchedulerWeb.Managers.Configuration;
using SchedulerWeb.Managers.Monitoring;
using SchedulerWeb.MeetupApi;
using SchedulerWeb.Repository;
using ConfigurationManager = SchedulerWeb.Managers.Configuration.ConfigurationManager;

namespace SchedulerWeb
{
    public class Startup
    {
        private const string DefaultAngularAntiforgeryTokenHeader = "X-XSRF-TOKEN";
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if (Environment.IsDevelopment())
            {
                services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration["DefaultDbConnection"]));
            }
            else
            {
                services.AddDbContext<ApplicationDbContext>(options => 
                options.UseMySql($"Server=mysql;Database={Configuration["MYSQL_DATABASE"]};Uid=root;Pwd={Configuration["MYSQL_ROOT_PASSWORD"]}"));
            }

            services.AddIdentity<ApplicationUser, IdentityRole>()
                    .AddEntityFrameworkStores<ApplicationDbContext>()
                    .AddDefaultTokenProviders();

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(cookieAuthenticationOptions =>
                    {
                        cookieAuthenticationOptions.LoginPath = "/Account/LogIn";
                        cookieAuthenticationOptions.LogoutPath = "/Account/LogOff";
                    })
                    .AddMeetup(meetupOptions =>
                    {
                        meetupOptions.ClientSecret = Configuration["MeetupClientSecret"];
                        meetupOptions.ClientId = Configuration["MeetupClientId"];
                    });

            services.AddAntiforgery(options =>
            {
                options.HeaderName = DefaultAngularAntiforgeryTokenHeader;
                options.Cookie.HttpOnly = false;
            });

            services.AddTransient<IEmailSender, MeetupScheduler.Web.Services.EmailSender>();
            services.AddTransient<IConfigurationManager, ConfigurationManager>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<ISchedulerBuilder, SchedulerBuilder>(CreateSchedulerBuilder);
            services.AddTransient<IMeetupUriGenerator, MeetupUriGenerator>(provider =>
            {
                var oauthToken = GetOauthToken(provider);
                return new MeetupUriGenerator(AuthenticationType.OAuth2, oauthToken);
            });
            services.AddTransient<IMeetupClient, MeetupClient>();

            services.AddSingleton<ISchedulerManager, SchedulerManager>();
            services.AddTransient<IAntiforgeryTokenProvider, AntiforgeryTokenProvider>();
            services.AddTransient<IMonitoringManager, MonitoringManager>();
            services.AddTransient<ISchedulerKeyGenerator, SchedulerKeyGenerator>();

            services.AddMvc(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
                options.Filters.Add(typeof(ValidateModelFilter));
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            });
            services.AddMemoryCache();
            services.AddTransient<IAsyncPolicy, PolicyWrap>(CreatePolicy);
            services.AddSingleton(LogManager.GetLogger(typeof(Program)));
            services.AddTransient<IMeetupUserGroupsRepository, MeetupUserGroupsRepository>();
            services.Decorate<IMeetupClient, MeetupClientDecorator>();

            services.AddDataProtection();
            services.AddTransient<ICipherService, CipherService>();
        }

        private static PolicyWrap CreatePolicy(IServiceProvider provider)
        {
            var memeoryCache = provider.GetService<IMemoryCache>();
            var memoryCacheProvider = new MemoryCacheProvider(memeoryCache);
            var cachePolicy = Policy.CacheAsync(memoryCacheProvider, new SlidingTtl(TimeSpan.FromMinutes(25)));

            var policy = Policy
                .Handle<HttpRequestException>()
                .WaitAndRetryAsync(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));

            return Policy.WrapAsync(cachePolicy, policy);
        }

        private SchedulerBuilder CreateSchedulerBuilder(IServiceProvider provider)
        {
            var oauthToken = GetOauthToken(provider);
            var sheduler = SchedulerBuilder.Create(oauthToken, AuthenticationType.OAuth2) as SchedulerBuilder;

            var email = new MailAddress(Configuration["Email"]);
            var credential = new NetworkCredential(Configuration["EmailUser"], Configuration["EmailPassword"]);

            var emailCredential = new EmailConfiguration(email, credential);
            sheduler.NotifyClientsWithCredentials(emailCredential);
            return sheduler;
        }

        private static string GetOauthToken(IServiceProvider provider)
        {
            var httpContextAccessor = provider.GetService<IHttpContextAccessor>();
            var authenticate = httpContextAccessor.HttpContext.AuthenticateAsync().GetAwaiter().GetResult();
            var accessToken = authenticate.Properties.Items.FirstOrDefault(x => x.Key.EndsWith("access_token"));
            return accessToken.Value;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory,
            IAntiforgery antiforgery,
            IServiceProvider provider)
        {
            provider.GetService<ApplicationDbContext>().Database.Migrate();
            loggerFactory.AddLog4Net("log4net.config");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });

                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvc(routes =>
             {
                 routes.MapRoute(
                     name: "default",
                     template: "{controller=Home}/{action=Index}/{id?}");

                 routes.MapSpaFallbackRoute(
                     name: "spa-fallback",
                     defaults: new { controller = "Home", action = "Index" });
             });
        }
    }
}
