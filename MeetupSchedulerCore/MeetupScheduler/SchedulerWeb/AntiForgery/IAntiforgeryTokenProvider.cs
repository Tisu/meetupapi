﻿using Microsoft.AspNetCore.Http;

namespace SchedulerWeb
{
    public interface IAntiforgeryTokenProvider
    {
        void AddAntiForgeryToken(HttpContext context);
    }
}