using System.Threading.Tasks;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Http;

namespace SchedulerWeb
{
    public class AntiforgeryTokenProvider : IAntiforgeryTokenProvider
    {
        private const string AntiForgeryToken = "XSRF-TOKEN";
        private readonly IAntiforgery _antiforgery;

        public AntiforgeryTokenProvider( IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void AddAntiForgeryToken(HttpContext context)
        {
            if (!context.Request.Cookies.Keys.Contains(AntiForgeryToken))
            {
                var tokens = _antiforgery.GetAndStoreTokens(context);
                context.Response.Cookies.Append(AntiForgeryToken, tokens.RequestToken, new CookieOptions { HttpOnly = false });
            }
        }
    }
}