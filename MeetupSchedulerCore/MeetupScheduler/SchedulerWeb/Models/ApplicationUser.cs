﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using SchedulerWeb.Database;
using SchedulerWeb.Dto.Configuration;

namespace MeetupScheduler.Web.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public IEnumerable<MeetupGroupMonitoring> MeetupGroupMonitorings { get; set; }
        public MeetupAdditionalConfiguration AdditionalConfiguration { get; set; }
    }
}
