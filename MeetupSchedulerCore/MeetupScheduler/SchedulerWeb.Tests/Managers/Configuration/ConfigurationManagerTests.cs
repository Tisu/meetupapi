﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using MeetupApi.MeetupClient;
using MeetupApiDto.GroupInformation;
using MeetupScheduler.Web.Models;
using NSubstitute;
using NUnit.Framework;
using Quartz;
using RsvpScheduler.Builder;
using RsvpScheduler.Manager;
using SchedulerWeb.Database;
using SchedulerWeb.Dto.Configuration;
using SchedulerWeb.Managers.Configuration;
using SchedulerWeb.Repository;

namespace SchedulerWeb.Tests.Managers.Configuration
{
    [TestFixture]
    public class ConfigurationManagerTests
    {
        private ConfigurationManager _configurationManager;
        private IMeetupClient _meetupClient;
        private IMeetupUserGroupsRepository _meetupUserGroupsRepository;

        [SetUp]
        public void Setup()
        {
            _meetupUserGroupsRepository = Substitute.For<IMeetupUserGroupsRepository>();
            _meetupClient = Substitute.For<IMeetupClient>();
            _configurationManager = new ConfigurationManager(Substitute.For<ISchedulerBuilder>(),
                                                             Substitute.For<ISchedulerManager>(),
                                                             _meetupClient,
                                                             _meetupUserGroupsRepository);
        }

        [Test]
        public async Task GetGroupsThatUserBelongsTo_GetConfigurationFromMeetup_WhenThereIsNoSavedConfigurationForUser()
        {
            _meetupUserGroupsRepository.GetUser(Arg.Any<string>()).Returns(Task.FromResult<ApplicationUser>(null));

            var meetupGroups = new List<GroupsDto>
            {
                new GroupsDto()
                {
                    GroupName = "test",
                    GroupUrl = "test"
                },
                new GroupsDto()
                {
                    GroupName = "test1",
                    GroupUrl = "test1"
                },
                new GroupsDto()
                {
                    GroupName = "test2",
                    GroupUrl = "test2"
                },
            };
            _meetupClient.GetGroupsThatUserBelongsTo().Returns(Task.FromResult(meetupGroups));

            var result = await _configurationManager.GetGroupsThatUserBelongsTo("ignore");

            result.GroupConfiguration.Should().HaveCount(meetupGroups.Count);
            var defaultMonitoringValue = result.GroupConfiguration.All(x => x.Monitoring);
            defaultMonitoringValue.Should().BeTrue();

            result.GroupConfiguration.ElementAt(0).Name.Should().Be(meetupGroups.ElementAt(0).GroupName);
            result.GroupConfiguration.ElementAt(1).Name.Should().Be(meetupGroups.ElementAt(1).GroupName);
            result.GroupConfiguration.ElementAt(2).Name.Should().Be(meetupGroups.ElementAt(2).GroupName);
        }

        [Test]
        public async Task GetGroupsThatUserBelongsTo_MergeInformationFromMeetupAndDatabase_WhenThereIsSavedConfigurationForUser()
        {
            var applicationUser = new ApplicationUser()
            {
                MeetupGroupMonitorings  = new List<MeetupGroupMonitoring>()
                {
                    new MeetupGroupMonitoring()
                    {
                        GroupName = "databaseMeetup",
                        Monitoring = false
                    },
                    new MeetupGroupMonitoring()
                    {
                        GroupName = "databaseMeetup1",
                        Monitoring = false
                    },
                    new MeetupGroupMonitoring()
                    {
                        GroupName = "databaseMeetup2",
                        Monitoring = false
                    },
                }
            };
            _meetupUserGroupsRepository.GetUser(Arg.Any<string>()).Returns(Task.FromResult<ApplicationUser>(applicationUser));

            var meetupGroups = new List<GroupsDto>
            {
                new GroupsDto()
                {
                    GroupName = "test",
                    GroupUrl = "test"
                },
                new GroupsDto()
                {
                    GroupName = "test1",
                    GroupUrl = "test1"
                },
                new GroupsDto()
                {
                    GroupName = "test2",
                    GroupUrl = "test2"
                },
            };
            _meetupClient.GetGroupsThatUserBelongsTo().Returns(Task.FromResult(meetupGroups));

            var result = await _configurationManager.GetGroupsThatUserBelongsTo("ignore");
            
            var expected = new ConfigurationDto()
            {
                GroupConfiguration = new List<GroupConfiguration>()
                {
                    new GroupConfiguration()
                    {
                        Monitoring = applicationUser.MeetupGroupMonitorings.ElementAt(0).Monitoring,
                        Name = applicationUser.MeetupGroupMonitorings.ElementAt(0).GroupName
                    },
                    new GroupConfiguration()
                    {
                        Monitoring = applicationUser.MeetupGroupMonitorings.ElementAt(1).Monitoring,
                        Name = applicationUser.MeetupGroupMonitorings.ElementAt(1).GroupName
                    },
                    new GroupConfiguration()
                    {
                        Monitoring = applicationUser.MeetupGroupMonitorings.ElementAt(2).Monitoring,
                        Name = applicationUser.MeetupGroupMonitorings.ElementAt(2).GroupName
                    },
                    new GroupConfiguration()
                    {
                        Monitoring = true,
                        Name = meetupGroups.ElementAt(0).GroupName
                    },
                    new GroupConfiguration()
                    {
                        Monitoring = true,
                        Name = meetupGroups.ElementAt(1).GroupName
                    },
                    new GroupConfiguration()
                    {
                        Monitoring = true,
                        Name = meetupGroups.ElementAt(2).GroupName
                    }
                },
            };
            result.GroupConfiguration.Should().BeEquivalentTo(expected.GroupConfiguration);
        }

        [Test]
        public async Task GetGroupsThatUserBelongsTo_MergeInformationFromMeetupAndDatabase_WhenSavedConfigurationIsEqualToGroupsOnMeetup()
        {
            var applicationUser = new ApplicationUser()
            {
                MeetupGroupMonitorings = new List<MeetupGroupMonitoring>()
                {
                    new MeetupGroupMonitoring()
                    {
                        GroupName = "test",
                        Monitoring = false
                    },
                    new MeetupGroupMonitoring()
                    {
                        GroupName = "test1",
                        Monitoring = false
                    },
                    new MeetupGroupMonitoring()
                    {
                        GroupName = "test2",
                        Monitoring = false
                    },
                }
            };
            _meetupUserGroupsRepository.GetUser(Arg.Any<string>()).Returns(Task.FromResult<ApplicationUser>(applicationUser));

            var meetupGroups = new List<GroupsDto>
            {
                new GroupsDto()
                {
                    GroupName = "test",
                    GroupUrl = "test"
                },
                new GroupsDto()
                {
                    GroupName = "test1",
                    GroupUrl = "test1"
                },
                new GroupsDto()
                {
                    GroupName = "test2",
                    GroupUrl = "test2"
                },
            };
            _meetupClient.GetGroupsThatUserBelongsTo().Returns(Task.FromResult(meetupGroups));

            var result = await _configurationManager.GetGroupsThatUserBelongsTo("ignore");

            var expected = new ConfigurationDto()
            {
                GroupConfiguration = new List<GroupConfiguration>()
                {
                    new GroupConfiguration()
                    {
                        Monitoring = applicationUser.MeetupGroupMonitorings.ElementAt(0).Monitoring,
                        Name = applicationUser.MeetupGroupMonitorings.ElementAt(0).GroupName
                    },
                    new GroupConfiguration()
                    {
                        Monitoring = applicationUser.MeetupGroupMonitorings.ElementAt(1).Monitoring,
                        Name = applicationUser.MeetupGroupMonitorings.ElementAt(1).GroupName
                    },
                    new GroupConfiguration()
                    {
                        Monitoring = applicationUser.MeetupGroupMonitorings.ElementAt(2).Monitoring,
                        Name = applicationUser.MeetupGroupMonitorings.ElementAt(2).GroupName
                    }
                }
            };
            result.GroupConfiguration.Should().BeEquivalentTo(expected.GroupConfiguration);
        }
    }
}