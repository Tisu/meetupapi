﻿using FluentAssertions;
using NUnit.Framework;
using RsvpScheduler.Common;

namespace RsvpScheduler.Tests.Common
{
    [TestFixture]
    public class StringExtenderTests
    {
        [Test]
        public void CompareWithoutPolishLettersIgnoreCase_CompareWithoutPolishLetters_Always()
        {
            "ąćęłńóśźż".CompareWithoutPolishLettersIgnoreCase("acelnoszz").Should().BeTrue();
        }

        [Test]
        public void CompareWithoutPolishLettersIgnoreCase_IgnoreCase_Always()
        {
            "ąćęłńóśźż".CompareWithoutPolishLettersIgnoreCase("ACELNOSZZ").Should().BeTrue();
        }
    }
}