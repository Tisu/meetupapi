﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetupApi.MeetupClient;
using MeetupEmailSender.Messages;
using NSubstitute;
using NUnit.Framework;
using Quartz;
using RsvpScheduler.Jobs;
using RsvpScheduler.QuartzKeys;
using RsvpScheduler.Scheduler;

namespace RsvpScheduler.Tests.Jobs
{
    [TestFixture]
    public class MeetupRsvpJobTests
    {
        private MeetupRsvpJob _sut;
        private IJobExecutionContext _context;
        private IMeetupClient _meetupClient;
        private ISuccessfullRsvpMessage _successfulMessageNotifier;
        private const string EventId = "EventId";

        [SetUp]
        public void SetUp()
        {
            _sut = new MeetupRsvpJob();
            _context = Substitute.For<IJobExecutionContext>();
            _meetupClient = Substitute.For<IMeetupClient>();
            _successfulMessageNotifier = Substitute.For<ISuccessfullRsvpMessage>();

            var dictionary = new Dictionary<string, object>
            {
                [MeetupJobKeys.MeetupClient] = _meetupClient,
                [MeetupJobKeys.EventId] = EventId,
                [MeetupJobKeys.SuccessfullMessage] = _successfulMessageNotifier
            };
            var jobData = new JobDataMap((IDictionary)dictionary);
            _context.MergedJobDataMap.Returns(jobData);
        }

        [Test]
        public async Task Execute_CallMeetupClient_Always()
        {
            await _sut.Execute(_context);

            await _meetupClient.Received().PostRsvpAsync(Arg.Any<string>());
        }

        [Test]
        public async Task Execute_CallMeetupClientWithCorrectEventId_Always()
        {
            await _sut.Execute(_context);

            await _meetupClient.Received().PostRsvpAsync(Arg.Is<string>(x => x == EventId));
        }

        [Test]
        public async Task Execute_SendSuccessfulRsvpNotification_Always()
        {
            _meetupClient.PostRsvpAsync(Arg.Any<string>()).Returns(true);
            await _sut.Execute(_context);

            _successfulMessageNotifier.Received().SendSuccessfulRsvpNotification();
        }
    }
}