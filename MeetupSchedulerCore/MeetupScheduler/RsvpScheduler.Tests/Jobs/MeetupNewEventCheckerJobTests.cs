﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Quartz;
using RsvpScheduler.Jobs;
using RsvpScheduler.QuartzKeys;
using RsvpScheduler.Scheduler;

namespace RsvpScheduler.Tests.Jobs
{
    [TestFixture]
    public class MeetupNewEventCheckerJobTests
    {
        public MeetupNewEventCheckerJob _sut = new MeetupNewEventCheckerJob();

        [Test]
        public async Task Execute_CallRsvpScheduler_Always()
        {
            var context = Substitute.For<IJobExecutionContext>();
            var rsvpScheduler = Substitute.For<IRsvpScheduler>();

            var dictionary = new Dictionary<string,object>{[MeetupJobKeys.RsvpScheduler] = rsvpScheduler };
            var jobData = new JobDataMap((IDictionary)dictionary);
            context.MergedJobDataMap.Returns(jobData);

            await _sut.Execute(context);

            rsvpScheduler.Received().ScheduleRsvpForNextMeeting();
        }
    }
}