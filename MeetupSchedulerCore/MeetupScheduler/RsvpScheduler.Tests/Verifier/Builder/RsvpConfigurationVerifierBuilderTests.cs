﻿using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using RsvpScheduler.Common;
using RsvpScheduler.Verifier;
using RsvpScheduler.Verifier.Builder;
using RsvpScheduler.Verifier.Proxy;

namespace RsvpScheduler.Tests.Verifier.Builder
{
    [TestFixture]
    public class RsvpConfigurationVerifierBuilderTests
    {
        private RsvpConfigurationVerifierBuilder _sut;
        private List<IRsvpConfigurationVerifier> _verifiers;

        [SetUp]
        public void SetUp()
        {
            _verifiers = new List<IRsvpConfigurationVerifier>();
            _sut = new RsvpConfigurationVerifierBuilder(_verifiers);
        }

        [Test]
        public void Build_ReturnVerifierProxy_Always()
        {
            var result = _sut.Build(default);

            result.Should().BeOfType<RsvpConfigurationVerifierProxy>();
        }

        [Test]
        public void Build_ReturnClosedRsvpVerifier_Always()
        {
            var result = _sut.Build(default);

            _verifiers.Should().HaveCount(1);
        }

        [TestCase(true, "", 2, TestName = "Build_WithDontSignForEventsInThePast_CreateTwoVerifiers")]
        [TestCase(false, "", 1, TestName = "Build_WithoutDontSignForEventsInThePast_CreateOneVerifier")]
        [TestCase(true, "Wroclaw", 3, TestName = "Build_WithDontSignForEventsInThePastAndCity_CreateThreeVerifiers")]
        [TestCase(false, "Wroclaw", 2, TestName = "Build_WithoutDontSignForEventsInThePast_CreateTwoVerifiers")]
        public void Build_ReturnClosedRsvpVerifier_Always(bool configuration, string city, int expectedVerifiers)
        {
            var result = _sut.Build(new SchedulerConfiguration(city, configuration));

            _verifiers.Should().HaveCount(expectedVerifiers);
        }
    }
}