﻿using FluentAssertions;
using MeetupApiDto.Event;
using NUnit.Framework;
using RsvpScheduler.Verifier.Implementation;

namespace RsvpScheduler.Tests.Verifier.Impementation
{
    [TestFixture]
    public class CityRsvpConfigurationVerifierTests
    {
        private CityRsvpConfigurationVerifier _sut;

        [Test]
        public void Verify_ReturnFalse_WhenCityIsIncorrect()
        {
            _sut = new CityRsvpConfigurationVerifier("wrong_city");

            var result = _sut.Verify(new EventDto() {Venue = new Venue() {City = "Wroclaw"}});

            result.Should().BeFalse();
        }
        [Test]
        public void Verify_ReturnFalse_WhenCityIsNotSet()
        {
            _sut = new CityRsvpConfigurationVerifier("ignore");

            var result = _sut.Verify(new EventDto() { Venue = null });

            result.Should().BeFalse();
        }
        [Test]
        public void Verify_ReturnTrue_WhenCityIsCorrect()
        {
            var city = "wroclaw";
            _sut = new CityRsvpConfigurationVerifier(city);

            var result = _sut.Verify(new EventDto() { Venue = new Venue() { City = city } });

            result.Should().BeTrue();
        }
        [Test]
        public void Verify_ReturnTrueIgnoringConfigurationCase_WhenCityIsCorrect()
        {
            var city = "Wroclaw";
            _sut = new CityRsvpConfigurationVerifier(city.ToLowerInvariant());

            var result = _sut.Verify(new EventDto() { Venue = new Venue() { City = city } });

            result.Should().BeTrue();
        }
        [Test]
        public void Verify_ReturnTrueIgnoringResponseCase_WhenCityIsCorrect()
        {
            var city = "Wroclaw";
            _sut = new CityRsvpConfigurationVerifier(city.ToLowerInvariant());

            var result = _sut.Verify(new EventDto() { Venue = new Venue() { City = city.ToLowerInvariant() } });

            result.Should().BeTrue();
        }
        [Test]
        public void Verify_ReturnTrueIgnoringPolishLetter_WhenCityIsCorrect()
        {
            var city = "ąęłśćżźóń";
            _sut = new CityRsvpConfigurationVerifier(city.ToLowerInvariant());

            var result = _sut.Verify(new EventDto() { Venue = new Venue() { City = "aelsczzon" } });

            result.Should().BeTrue();
        }
    }
}