﻿using System;
using FluentAssertions;
using MeetupApiDto.Event;
using NUnit.Framework;
using RsvpScheduler.Verifier.Implementation;

namespace RsvpScheduler.Tests.Verifier.Impementation
{
    public class EventInThePastRsvpConfigurationVerifierTests
    {
        private EventInThePastRsvpConfigurationVerifier _sut = new EventInThePastRsvpConfigurationVerifier();

        [Test]
        public void Verify_ReturnFalse_WhenEventIsInThePast()
        {
            var result = _sut.Verify(new EventDto()
            {
                RsvpRules = new RsvpRules()
                {
                    OpenTime = (long) (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds -1
                }
            });

            result.Should().BeFalse();
        }
        [Test]
        public void Verify_ReturnTrue_WhenEventIsNotInThePast()
        {
            var result = _sut.Verify(new EventDto()
            {
                RsvpRules = new RsvpRules()
                {
                    OpenTime = (long)(new DateTime(2037, 1, 1).Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds
                }
            });

            result.Should().BeTrue(); 
        }
        [Test]
        public void IsEventInThePast_ReturnTrue_WhenEventIsInThePast()
        {
            var result = _sut.IsEventInThePast(new EventDto()
            {
                RsvpRules = new RsvpRules()
                {
                    OpenTime = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds - 1
                }
            });

            result.Should().BeTrue();
        }
        [Test]
        public void IsEventInThePast_ReturnFalse_WhenEventIsInTheFuture()
        {
            var result = _sut.IsEventInThePast(new EventDto()
            {
                RsvpRules = new RsvpRules()
                {
                    OpenTime = (long)(new DateTime(2037, 1, 1).Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds
                }
            });

            result.Should().BeFalse();
        }
    }
}