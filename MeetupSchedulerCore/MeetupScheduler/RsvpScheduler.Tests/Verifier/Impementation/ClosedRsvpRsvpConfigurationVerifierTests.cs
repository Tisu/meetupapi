﻿using FluentAssertions;
using MeetupApiDto.Event;
using NUnit.Framework;
using RsvpScheduler.Verifier.Implementation;

namespace RsvpScheduler.Tests.Verifier.Impementation
{
    [TestFixture]
    public class ClosedRsvpRsvpConfigurationVerifierTests
    {
        private ClosedRsvpRsvpConfigurationVerifier _sut = new ClosedRsvpRsvpConfigurationVerifier();

        [Test]
        public void Verify_ReturnFalse_WhenEventIsClosed()
        {
            var result = _sut.Verify(new EventDto() {RsvpRules = new RsvpRules() {Closed = true}});

            result.Should().BeFalse();
        }

        [Test]
        public void Verify_ReturnTrue_WhenEventIsNotClosed()
        {
            var result = _sut.Verify(new EventDto() { RsvpRules = new RsvpRules() { Closed = false } });

            result.Should().BeTrue();
        }
    }
}