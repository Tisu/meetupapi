﻿using System.Collections.Generic;
using FluentAssertions;
using MeetupApiDto.Event;
using NSubstitute;
using NUnit.Framework;
using RsvpScheduler.Verifier;
using RsvpScheduler.Verifier.Proxy;

namespace RsvpScheduler.Tests.Verifier.Proxy
{
    [TestFixture]
    public class VerifierProxyTests
    {
        private RsvpConfigurationVerifierProxy _sut;
        private List<IRsvpConfigurationVerifier> _verifiers;

        [SetUp]
        public void SetUp()
        {
            _verifiers = new List<IRsvpConfigurationVerifier>();
            _sut = new RsvpConfigurationVerifierProxy(_verifiers);
        }

        [Test]
        public void Verify_CallEveryVerifier_Always()
        {
            GenerateVerifiers();

            var eventDto = new EventDto();
            _sut.Verify(eventDto);

            foreach (var verifier in _verifiers)
            {
                verifier.Received(1).Verify(eventDto);
            }
        }
        [Test]
        public void Verify_ReturnTrue_AllReturnTrue()
        {
            GenerateVerifiers();

            var eventDto = new EventDto();
            var result = _sut.Verify(eventDto);

            result.Should().BeTrue();
        }

        [Test]
        public void Verify_ReturnFalse_AtLeastOneVerifierReturnFalse()
        {
            GenerateVerifiers();

            var verifier = Substitute.For<IRsvpConfigurationVerifier>();
            verifier.Verify(Arg.Any<EventDto>()).Returns(false);
            _verifiers.Add(verifier);

            var eventDto = new EventDto();
            var result = _sut.Verify(eventDto);

            result.Should().BeFalse();
        }

        private void GenerateVerifiers()
        {
            for (int i = 0; i < 10; i++)
            {
                var verifier = Substitute.For<IRsvpConfigurationVerifier>();
                verifier.Verify(Arg.Any<EventDto>()).Returns(true);
                _verifiers.Add(verifier);
            }
        }
    }
}