﻿using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using Quartz;
using RsvpScheduler.Proxy;
using RsvpScheduler.Scheduler;

namespace RsvpScheduler.Tests.Proxy
{
    public class RsvpSchedulerProxyTests
    {
        private RsvpSchedulerProxy _sut;
        private IScheduler _scheduler;
        private List<IRsvpScheduler> _rsvpSchedulers;

        [SetUp]
        public void SetUp()
        {
            _scheduler = Substitute.For<IScheduler>();
            _rsvpSchedulers = new List<IRsvpScheduler>();
            for (int i = 0; i < 10; i++)
            {
                _rsvpSchedulers.Add(Substitute.For<IRsvpScheduler>());
            }

            _sut = new RsvpSchedulerProxy(_scheduler,_rsvpSchedulers);
        }

        [Test]
        public void ScheduleRsvpForNextMeeting_CallEveryScheduler_Always()
        {
            _sut.ScheduleRsvpForNextMeeting();

            foreach (var scheduler in _rsvpSchedulers)
            {
                scheduler.Received(1).ScheduleRsvpForNextMeeting();
            }
        }

        [Test]
        public void Dispose_CallEveryRsvpScheduler_Always()
        {
            _sut.Dispose();

            foreach (var scheduler in _rsvpSchedulers)
            {
                scheduler.Received(1).Dispose();
            }
        }
        [Test]
        public void Dispose_ShutdownScheduler_Always()
        {
            _sut.Dispose();

            _scheduler.Received(1).Shutdown();
        }
    }
}