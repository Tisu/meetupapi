FROM microsoft/aspnetcore-build:2.0 as build-env
WORKDIR /app
# copy csproj and restore as distinct layers
COPY . .

RUN dotnet restore MeetupScheduler.sln
RUN dotnet publish MeetupScheduler.sln -o out /p:PublishWithAspNetCoreTargetManifest="false" -c release

# Build runtime image
FROM microsoft/dotnet:2.0-runtime-stretch-arm32v7 AS runtime
ENV ASPNETCORE_URLS http://+:80

RUN apt-get -qq update && apt-get -qqy --no-install-recommends install wget gnupg \
    git \
    unzip

RUN curl -sL https://deb.nodesource.com/setup_6.x |  bash -
RUN apt-get install -y nodejs

WORKDIR /app
COPY --from=build-env /app/SchedulerWeb/out ./

ENTRYPOINT ["dotnet", "SchedulerWeb.dll"]