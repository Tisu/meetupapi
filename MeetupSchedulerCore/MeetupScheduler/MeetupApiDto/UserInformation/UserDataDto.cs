﻿using Newtonsoft.Json;

namespace MeetupApiDto.UserInformation
{
    public sealed class UserDataDto
    {
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
    }
}