﻿using Newtonsoft.Json;

namespace MeetupApiDto.GroupInformation
{
    public sealed class GroupsDto
    {
        [JsonProperty(PropertyName = "name")]
        public string GroupName;

        [JsonProperty(PropertyName = "urlname")]
        public string GroupUrl;
    }
}