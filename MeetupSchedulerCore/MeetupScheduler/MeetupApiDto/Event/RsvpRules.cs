﻿using Newtonsoft.Json;

namespace MeetupApiDto.Event
{
    [JsonObject(Title = "rsvp_rules")]
    public sealed class RsvpRules
    {
        [JsonProperty(PropertyName = "open_time")]
        public long OpenTime { get; set; }
        [JsonProperty(PropertyName = "closed")]
        public bool Closed { get; set; }
        [JsonProperty(PropertyName = "guest_limit")]
        public int GuestLimit { get; set; }
    }
}