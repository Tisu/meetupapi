﻿using Newtonsoft.Json;

namespace MeetupApiDto.Event
{
    [JsonObject(Title = "venue")]
    public sealed class Venue
    {
        [JsonProperty(PropertyName = "city")]
        public string City;
    }
}