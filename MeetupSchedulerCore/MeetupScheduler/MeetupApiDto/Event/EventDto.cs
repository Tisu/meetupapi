﻿using MeetupApiDto.GroupInformation;
using Newtonsoft.Json;

namespace MeetupApiDto.Event
{
    public sealed class EventDto
    {
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string EventId { get; set; }

        [JsonProperty(PropertyName = "utc_offset")]
        public long UtcOffset { get; set; }

        [JsonProperty(PropertyName = "rsvp_rules")]
        public RsvpRules RsvpRules { get; set; }

        [JsonProperty(PropertyName = "venue")]
        public Venue Venue { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "link")]
        public string Link { get; set; }

        [JsonProperty(PropertyName = "time")]
        public long EventStartTime { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "group")]
        public GroupsDto Group { get; set; }
    }
}