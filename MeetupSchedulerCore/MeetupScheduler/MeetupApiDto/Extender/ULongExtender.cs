﻿using System;

namespace MeetupApiDto.Extender
{
    public static class ULongExtender
    {
        public static DateTime GetDateTimeFromUnixTimestamp(this long timestamp, long utcOffset)
        {
            var date = TimeSpan.FromMilliseconds(timestamp);
            var timeZone = TimeSpan.FromMilliseconds(utcOffset);
            return new DateTime(1970, 1, 1) + date + timeZone;
        }
        public static DateTime GetDateTimeFromUnixTimestampUtc(this long timestamp)
        {
            return DateTimeOffset.FromUnixTimeMilliseconds(timestamp).DateTime;
        }
    }
}