﻿namespace MeetupApi.MeetupUri
{
    public interface IMeetupUriGenerator
    {
        string GetPostRsvpUrl(string eventId);
        string GetUserDataUrl();
        string GetUserGroupsUrl();
        string GetGetRsvpRulesUrl(string groupName);
    }
}