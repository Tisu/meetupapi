﻿using MeetupApi.MeetupClient;
using MeetupApi.MeetupClient.Requests;

namespace MeetupApi.MeetupUri
{
    public class MeetupUriGenerator : IMeetupUriGenerator
    {
        private readonly AuthenticationType _authenticationType;
        private readonly string _key;

        public MeetupUriGenerator(AuthenticationType authenticationType, string key)
        {
            _authenticationType = authenticationType;
            _key = key;
        }

        public string GetUserDataUrl()
        {
            return string.Format(MeetupApiRequests.UserData, GetAuthenticationParameter(), _key);
        }

        public string GetUserGroupsUrl()
        {
            return string.Format(MeetupApiRequests.UserGroups, GetAuthenticationParameter(), _key);
        }

        public string GetPostRsvpUrl(string eventId)
        {
            return string.Format(MeetupApiRequests.PostRsvp, GetAuthenticationParameter(), _key, eventId);
        }

        public string GetGetRsvpRulesUrl(string groupName)
        {
            return $@"{groupName}{MeetupApiRequests.Rsvp}";
        }

        private string GetAuthenticationParameter()
        {
            return _authenticationType == AuthenticationType.ApiKey ? "key" : "access_token";
        }
    }
}