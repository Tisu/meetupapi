﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using log4net;
using MeetupApi.MeetupClient.Requests;
using MeetupApi.MeetupUri;
using MeetupApiDto.Event;
using MeetupApiDto.GroupInformation;
using MeetupApiDto.UserInformation;
using Newtonsoft.Json;

namespace MeetupApi.MeetupClient
{
    public sealed class MeetupClient : IMeetupClient
    {
        private readonly ILog _logger;
        private readonly HttpClient _httpClient;
        private readonly IMeetupUriGenerator _meetupUriGenerator;

        static MeetupClient()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }
        public MeetupClient(ILog logger, IMeetupUriGenerator meetupUriGenerator)
        {
            _logger = logger;
            _meetupUriGenerator = meetupUriGenerator;
            _httpClient = new HttpClient()
            {
                BaseAddress = BaseUri.BaseUri.MeetupUri
            };
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<GroupsDto>> GetGroupsThatUserBelongsTo()
        {
            using(var response = await _httpClient.GetAsync(_meetupUriGenerator.GetUserGroupsUrl()))
            {
                if(!response.IsSuccessStatusCode)
                {
                    _logger.Error("GetGroupsThatUserBelongsTo failed!");
                    return new List<GroupsDto>();
                }

                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<GroupsDto>>(jsonString);
            }
        }

        public async Task<UserDataDto> GetUserData()
        {
            using (var response = await _httpClient.GetAsync(_meetupUriGenerator.GetUserDataUrl()))
            {
                if (!response.IsSuccessStatusCode)
                {
                    _logger.Error("GetUserData failed!");
                    return new UserDataDto();
                }

                var jsonString = await response.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<UserDataDto>(jsonString);
            }
        }
        public async Task<List<EventDto>> GetRsvpRulesAsync(string groupName)
        {
            using(var response = await _httpClient.GetAsync(_meetupUriGenerator.GetGetRsvpRulesUrl(groupName)))
            {
                if(!response.IsSuccessStatusCode)
                {
                    _logger.Error("GetRsvpRulesAsync failed!");
                    return new List<EventDto>();
                }

                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<EventDto>>(jsonString);
            }
        }

        public async Task<bool> PostRsvpAsync(string eventId)
        {
            var parameters = _meetupUriGenerator.GetPostRsvpUrl(eventId);
            var uri = new Uri($@"{_httpClient.BaseAddress}{parameters}");
            var content = new StringContent("", Encoding.UTF8, "application/json");
            using(var response = await _httpClient.PostAsync(uri, content))
            {
                return response.IsSuccessStatusCode;
            }
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }
}