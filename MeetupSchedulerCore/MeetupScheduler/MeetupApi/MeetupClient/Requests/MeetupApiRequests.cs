﻿namespace MeetupApi.MeetupClient.Requests
{
    public static class MeetupApiRequests
    {
        public const string Rsvp = @"/events?&sign=true&photo-host=public&page=20&fields=rsvp_rules";

        public static readonly string UserGroups = @"/self/groups?{0}={1}&only=name,urlname";
        public static readonly string UserData = @"members/self?{0}={1}&only=city,name,email";
        public static readonly string PostRsvp = @"/rsvp?{0}={1}&sign=true&rsvp=yes&event_id={2}";
    }
}