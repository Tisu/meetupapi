﻿namespace MeetupApi.MeetupClient
{
    public enum AuthenticationType
    {
        OAuth2,
        ApiKey
    }
}