﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MeetupApiDto.Event;
using MeetupApiDto.GroupInformation;

namespace MeetupApi.MeetupClient
{
    public interface IMeetupClient : IDisposable
    {
        Task<List<EventDto>> GetRsvpRulesAsync(string groupName);
        Task<List<GroupsDto>> GetGroupsThatUserBelongsTo();
        Task<bool> PostRsvpAsync(string eventId);
    }
}