﻿using System;

namespace MeetupApi.BaseUri
{
    public static class BaseUri
    {
        public static readonly Uri MeetupUri = new Uri(@"https://api.meetup.com/2");
    }
}